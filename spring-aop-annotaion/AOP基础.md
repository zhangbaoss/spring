# AOP基础 #

## 1.什么是AOP

传统的OOP开发中的代码逻辑是自上至下的,而这些过程会产生一些横切性的问题,这些横切性的问题与我们的业务逻辑关系不大,并不会影响主逻辑的实现,但是会散落到代码的各个部分,难以维护.
AOP是处理一些横切性问题,AOP编程思想就是将这些问题和主业务逻辑分开,达到与主业务逻辑解耦的目的,使代码的重用性和开发效率更高.

## 2.AOP的应用场景
* 日志记录
* 权限校验
* 效率检查
* 事务管理
* 异常处理
* ...

## 3.Spring Aop与AspectJ的关系

SpringAop和AspectJ都是Aop的实现产品,类似与DI与IOC的关系,IOC是一种理念,DI是IOC理念的产品实现
SpringAop和AspectJ就是AOP理念的产品实现.SpringAOP有自己的语法,但语法复杂,所以SpringAop借鉴了AspectJ的注解,但底层还是自己的

## 4.SpringAop属性基本概念

1. aspect: 切面,
2. Join point: 连接点,在spring中总是表示执行的方法
3. Advice: 通知,方便再特定的连接点上的操作
4. Pointcut: 切点, 多个连接点组成一个切点
5. Introduction: 使一个类动态实现一个接口
6. Target object: 原对象,被代理的对象
7. AOP proxy: 代理对象,再spring中代理对象由JDK动态代理或者CGLIB动态代理

## 5.advice通知类型:
1. Before 连接点执行之前，但是无法阻止连接点的正常执行，除非该段执行抛出异常
2. After  连接点正常执行之后，执行过程中正常执行返回退出，非异常退出
3. After throwing  执行抛出异常的时候
4. After (finally)  无论连接点是正常退出还是异常退出，都会执行
5. Around advice: 围绕连接点执行，例如方法调用。这是最有用的切面方式。around通知可以在方法调用之前和之后执行自定义行为。它还负责选择是继续加入点还是通过返回自己的返回值或抛出异常来快速建议的方法执行。

## 6.ProceedingJoinPoint 和JoinPoint的区别
ProceedingJoinPoint继承了JoinPoint,proceed()这个是aop代理链执行的方法。并扩充实现了proceed()方法，用于继续执行连接点。JoinPoint仅能获取相关参数，无法执行连接点。

### JoinPoint的方法
1. java.lang.Object[] getArgs()：获取连接点方法运行时的入参列表； 
2. Signature getSignature() ：获取连接点的方法签名对象； 
3. java.lang.Object getTarget() ：获取连接点所在的目标对象； 
4. java.lang.Object getThis() ：获取代理对象本身；
5. proceed()有重载,有个带参数的方法,可以修改目标方法的的参数