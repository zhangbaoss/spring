package com.zhangbao;

import com.zhangbao.calculator.MathCalculator;
import com.zhangbao.proxy.custom.CustomInvocationHandler;
import com.zhangbao.proxy.custom.CustomProxyClass;
import com.zhangbao.proxy.custom.CustomProxyInterface;
import com.zhangbao.proxy.custom.CustomProxyUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Hello world!
 *
 */
public class App implements CustomInvocationHandler {

    private Object target;

    public App(Object target) {
        this.target = target;
    }

    public static void main(String[] args ) throws Exception {
        CustomProxyInterface instance = (CustomProxyInterface)
                CustomProxyUtil.newInstance(new CustomProxyClass(), new App(new CustomProxyClass()));
        assert instance != null;
        String aaa = instance.test3("abcdefg", 3);
        System.out.println(aaa);
    }

    @Override
    public Object invoke(Method method, Object[] args) {
        try {
            System.out.println("添加前置方法...");
            Object res =  method.invoke(target, args);
            System.out.println("添加后置方法...");
            return res;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
