package com.zhangbao.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.Arrays;

/**
 * 切面类
 *	@Aspect:告诉Spring容器这是一个切面类
 * @author zhangbao
 * @create 2019/2/28
 * @since 1.0.0
 */
@Aspect
public class AnnotationAspects {

	/**
	 * 切点:
	 * 	public:切点的修饰符
	 * 	int:切点返回值
	 * 	com.xxx:切点的包位置
	 * 	*:该类下的所有方法
	 * 	..:方法的参数列表任意
	 */
	@Pointcut("execution(public int com.zhangbao.calculator.MathCalculator.*(..))")
	public void pointCut(){}

	/**
	 * 该切面类中引用切点直接使用pointCut()
	 */
	@Before("pointCut()")
	public void before(JoinPoint joinPoint){
		System.out.println("方法运行前运行, 方法参数为: " + Arrays.asList(joinPoint.getArgs()));
	}

	/**
	 * 假如是外部类引用该类的切点:
	 * 	com.zhangbao.aspect.AnnotationAspects.pointCut()
	 */
	@After("com.zhangbao.aspect.AnnotationAspects.pointCut()")
	public void after(JoinPoint joinPoint) {
		System.out.println("方法名为 :" +joinPoint.getSignature().getName() + "运行后运行,无论方法运行有没有发生异常");
	}

	/**
	 * 如果有多个参数,JoinPoint必须要在第一位
	 * @param joinPoint
	 * @param result
	 */
	@AfterReturning(pointcut = "pointCut()", returning = "result")
	public void afterReturning(JoinPoint joinPoint, int result) {
		System.out.println("方法名为 :" +joinPoint.getSignature().getName() + "正常返回后运行, 返回值 : " + result);
	}

	@AfterThrowing(pointcut = "pointCut()", throwing = "e")
	public void afterThrowing(Exception e) {
		System.out.println("方法抛出异常后运行, 异常为 : " + e);
	}

	@Around("pointCut()")
	public void around(ProceedingJoinPoint joinPoint) throws Throwable {
		Object proceed = joinPoint.proceed();
	}
}
