package com.zhangbao.proxy.stative;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/5/20 下午 3:38
 * @Version 1.0
 **/
public class StativeRealClass implements StativeInterface {
    @Override
    public void exec() {
        System.out.println("真实实现StativeInterface接口的类...");
    }
}
