package com.zhangbao.proxy.stative;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/5/20 下午 3:30
 * @Version 1.0
 **/
public interface StativeInterface {

    void exec();
}
