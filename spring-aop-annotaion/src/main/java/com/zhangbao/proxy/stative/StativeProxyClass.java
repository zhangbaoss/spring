package com.zhangbao.proxy.stative;

/**
 * @Description 静态代理
 * 静态代理只能代理一个具体的类，如果要代理一个接口的多个实现的话需要定义不同的代理类
 * @Author zhangbao
 * @Date 2019/5/20 下午 3:11
 * @Version 1.0
 **/
public class StativeProxyClass implements StativeInterface {

    private StativeInterface stativeInterface;

    public StativeProxyClass() {
        this.stativeInterface = new StativeRealClass();
    }

    @Override
    public void exec() {
        System.out.println("真实类执行exec()之前");
        stativeInterface.exec();
        System.out.println("真实类执行exec()之后");
    }
}
