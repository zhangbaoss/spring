package com.zhangbao.proxy.dynamic;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/5/20 下午 5:39
 * @Version 1.0
 **/
public interface DynamicMethod {

    void dynamic(String name);
}
