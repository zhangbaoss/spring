package com.zhangbao.proxy.dynamic;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/5/20 下午 5:38
 * @Version 1.0
 **/
public class DynamicMethodImpl implements DynamicMethod {

    @Override
    public void dynamic(String name) {
        System.out.println("执行dynamic(), name为:" + name);
    }
}
