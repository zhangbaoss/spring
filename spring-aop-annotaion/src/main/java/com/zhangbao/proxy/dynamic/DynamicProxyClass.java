package com.zhangbao.proxy.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Description 动态代理类
 * @Author zhangbao
 * @Date 2019/5/20 下午 4:21
 * @Version 1.0
 **/
public class DynamicProxyClass implements InvocationHandler {

    private Object target;

    public Object newProxyInstance(Class cls) {
        try {
            this.target = cls.newInstance();
        } catch (InstantiationException e) {
            System.out.println("InstantiationException异常");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.out.println("IllegalAccessException异常");
            e.printStackTrace();
        }
        /**
         * 为指定类装载器,一组接口及调用处理器生成动态代理实例
         * @param loader 指定产生代理对象的类加载器,需要将其指定为和目标对象同一个类加载器
         * @param interfaces 要实现和目标对象一样的接口,所以只需要拿到目标对象的实现接口
         * @param this 表明这些被拦截的方法在被拦截时需要执行哪个InvocationHandler的invoke()
         * @return 根据传入的目标返回一个代理对象
         */
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
    }

    /**
     * 关联这个实现类的方法被调用时将被执行
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //方法执行前执行的语句
        System.out.println("方法执行前执行的语句");
        //方法执行,此处第一个参数不能是proxy导致死循环
        Object result = method.invoke(target, args);

        //方法执行后执行的语句
        System.out.println("方法执行后执行的语句");
        return result;
    }
}
