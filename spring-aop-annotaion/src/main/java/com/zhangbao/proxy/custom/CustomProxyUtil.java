package com.zhangbao.proxy.custom;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @Description 使用字符串构造java类,并将其编译为class文件,加载到项目中,生成动态代理
 * @Author zhangbao
 * @Date 2019/12/11 9:35
 * @Version 1.0
 **/
public class CustomProxyUtil {

    public static Object newInstance(Object target, CustomInvocationHandler handler) {
        // 1.获取传入的对象的接口所在包
        Class<?> targetInf = target.getClass().getInterfaces()[0];
        // 2.获取字符串写成的类
        String proxyContent = getClassString(targetInf, handler);

        File file = new File("D:\\com\\zhangbao\\proxy\\$CustomProxy.java");
        try {
            // 3.将String字符串写入java文件
            writerToJava(proxyContent, file);

            // 4.将java文件编译成class文件
            compilerJavaToClass(file);

            // 5.加载class文件到项目并返回对象
            final Object object = loaderClass(handler);
            file.delete();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 使用字符串构造类
     * @param targetInf
     * @param handler
     * @return
     */
    private static String getClassString(Class<?> targetInf, CustomInvocationHandler handler) {
        // 获取原对象接口名类型
        String targetInfNameType = targetInf.getSimpleName();
        String line = "\n";
        String tab = "\t";
        // 构造包信息
        String packageContent = "package com.zhangbao.proxy;" + line;
        // 构造导入信息 // 导入InvocationHandler包
        String importContent = "import " + targetInf.getName() + ";" + line;
               importContent += "import com.zhangbao.proxy.custom.CustomInvocationHandler;" + line;
               // 导入Method包
               importContent += "import java.lang.reflect.Method;" + line;
               importContent += "import java.lang.Exception;" + line;
        // 构造类第一行信息
        String classFirstContext = "public class $CustomProxy implements " + targetInfNameType + " {" + line;
        // 构造参数信息
        String filedContent = tab + "private CustomInvocationHandler handler;" + line;
        // 构造构造函数信息
        String constructorContent = tab + "public $CustomProxy (CustomInvocationHandler handler) {" + line
                + tab + tab + "this.handler = handler;" + line
                + tab + "}" + line;
        String methodContent = "";
        // 获取接口的所有方法
        Method[] methods = targetInf.getDeclaredMethods();
        for (Method method : methods) {
            // 获取方法的返回值
            String returnTypeName = method.getReturnType().getSimpleName();
            // 获取方法名
            String methodName =method.getName();
            // 获取所有参数类型
            Class[] args = method.getParameterTypes();
            String argsContent = "";
            String paramsType = "";
            String paramsContent = "";
            for (int i = 0; i < args.length; i++) {
                // 获取每个参数类型,注意如果方法参数不是基本类型或java.lang包下的类型需要在import上导入
                String argNameType = args[i].getSimpleName();
                if (!basicType(argNameType)) {
                    String className = args[i].getName();
                    if (!javaLangType(className)) {
                        importContent += "import " + className + ";" + line;
                    }
                }
                // 构造方法参数
                argsContent += argNameType + " param" + i + ", ";
                // 构造获取method方法的参数
                paramsType += argNameType + ".class" + ", ";
                // 构造调用invoke方法的参数
                paramsContent += "param" + i + ", ";
            }

            // 获取类定义信息
            String classDefine = "Class clazz = Class.forName(\"" + targetInf.getName() + "\");";
            // 获取方法定义信息
            String methodDefine = "Method method = clazz.getDeclaredMethod(\"" + methodName + "\"";

            // 如果有方法参数,则将最后一个","去除
            if (argsContent.length() > 0) {
                argsContent = argsContent.substring(0, argsContent.lastIndexOf(", "));
                paramsType = paramsType.substring(0, paramsType.lastIndexOf(", "));
                paramsContent = paramsContent.substring(0, paramsContent.lastIndexOf(", "));
                // 将参数类型传递给获取method的方法
                methodDefine += ", " + paramsType + ");";
            } else {
                // 如果没有参数,不需要传递参数类型给获取Method的方法
                methodDefine += ");";
            }

            // 如果没有参数,传递空数组
            String returnContent = "handler.invoke(method, "
                    + ("".equals(paramsContent) ? "new Object[]{}" : "new Object[]{" + paramsContent +"}") + ");";
            if (!"void".equals(returnTypeName)) {
                returnContent = "return (" + returnTypeName + ")" + returnContent;
            }
            // 构造方法体
            methodContent += tab + "@Override" + line
                    + tab + "public " + returnTypeName + " " + methodName + " (" + argsContent + ") {" + line
                    + tab + tab + "try {" + line
                    + tab + tab + tab + classDefine + line
                    + tab + tab + tab + methodDefine + line
                    + tab + tab + tab + returnContent + line
                    + tab + tab + "} catch (Exception e){" + line
                    + tab + tab + tab + "e.printStackTrace();"
                    + ("void".equals(returnTypeName) ? line : (line + tab + tab + tab + "return null;" + line))
                    + tab + tab + "}" + line
                    + tab + "}" + line;
        }
        // 构造类最后一行信息
        String classLastContent = "}";
        return packageContent + line + importContent + line + classFirstContext + line
               + filedContent + line + constructorContent + line
               + methodContent + line + classLastContent;
    }

    /**
     * 将字符串写入.java文件
     * @param proxyContent
     * @param file
     */
    private static void writerToJava(String proxyContent, File file) {
        try(FileWriter fw = new FileWriter(file)) {
            if (!file.exists()) {
                file.createNewFile();
            }
            fw.write(proxyContent);
            fw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将java文件编译为class文件
     * @param file
     * @throws IOException
     */
    private static void compilerJavaToClass(File file) throws IOException {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileMgr = compiler.getStandardFileManager(null, null, null);
        Iterable units = fileMgr.getJavaFileObjects(file);
        JavaCompiler.CompilationTask t = compiler.getTask(null, fileMgr, null, null, null, units);
        t.call();
        fileMgr.close();
    }

    /**
     * 加载类
     * @return
     * @throws Exception
     */
    private static Object loaderClass(CustomInvocationHandler handler) throws Exception {
        URL[] urls = new URL[]{new URL("file:D:\\\\")};
        // 由于文件不在项目中,所以使用URLClassLoader类加载器
        URLClassLoader urlClassLoader = new URLClassLoader(urls);
        Class<?> clazz = urlClassLoader.loadClass("com.zhangbao.proxy.$CustomProxy");
        Constructor<?> constructor = clazz.getConstructor(CustomInvocationHandler.class);
        // 没有默认的构造方法,需要调用自定义构造方法
        return constructor.newInstance(handler);
    }

    private static boolean basicType(String argType) {
        switch (argType) {
            case "int":
            case "short":
            case "byte":
            case "boolean":
            case "float":
            case "double":
            case "long":
                return true;
            default: return false;
        }
    }

    private static boolean javaLangType(String className) {
        return className.contains("java.lang");
    }
}
