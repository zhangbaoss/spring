package com.zhangbao.proxy.custom;

import java.lang.reflect.Method;

/**
 * @Description 作用类似于JDK的InvocationHandler
 * @Author zhangbao
 * @Date 2019/12/11 13:51
 * @Version 1.0
 **/
public interface CustomInvocationHandler {
    /**
     * 执行方法
     * @param method 目标对象需要被执行的方法
     * @param args 方法的参数
     * @return
     */
    Object invoke(Method method, Object[] args);
}
