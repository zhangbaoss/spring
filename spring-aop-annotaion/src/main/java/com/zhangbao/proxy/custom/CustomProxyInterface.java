package com.zhangbao.proxy.custom;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/11 9:50
 * @Version 1.0
 **/
public interface CustomProxyInterface {
    /**
     * 测试方法1
     */
    void test1();

    /**
     * 测试方法2
     * @param param
     * @return
     */
    String test2(String param);

    /**
     * 测试方法3
     * @param param
     * @return
     */
    String test3(String param, int a);
}
