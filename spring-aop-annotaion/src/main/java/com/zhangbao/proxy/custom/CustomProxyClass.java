package com.zhangbao.proxy.custom;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/11 9:49
 * @Version 1.0
 **/
public class CustomProxyClass implements CustomProxyInterface {

    @Override
    public void test1() {
        System.out.println("void test1()");
    }

    @Override
    public String test2(String param) {
        System.out.println("String test2()");
        return param;
    }

    @Override
    public String test3(String param, int a) {
        System.out.println("String test2(p1, p2)");
        return param + a;
    }

}
