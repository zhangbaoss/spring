package com.zhangbao;

import static org.junit.Assert.assertTrue;

import com.zhangbao.config.AnnotationAopConfig;
import com.zhangbao.calculator.MathCalculator;
import com.zhangbao.proxy.dynamic.DynamicMethod;
import com.zhangbao.proxy.dynamic.DynamicMethodImpl;
import com.zhangbao.proxy.dynamic.DynamicProxyClass;
import com.zhangbao.proxy.stative.StativeInterface;
import com.zhangbao.proxy.stative.StativeProxyClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    private AnnotationConfigApplicationContext aopContext;

    @Test
    public void testAop() {
        aopContext = new AnnotationConfigApplicationContext(AnnotationAopConfig.class);
        MathCalculator calculator = aopContext.getBean(MathCalculator.class);
        calculator.div(1, 1);
        aopContext.close();
    }

    @Test
    public void testStativeProxy() {
        StativeInterface proxyClass = new StativeProxyClass();
        proxyClass.exec();
    }

    @Test
    public void testDynamicProxy() {
        DynamicProxyClass proxy = new DynamicProxyClass();
        DynamicMethod method = (DynamicMethod) proxy.newProxyInstance(DynamicMethodImpl.class);
        method.dynamic("testDynamicProxy");
//        DynamicMethod method1 = new DynamicMethodImpl();
//        DynamicMethod method2 = (DynamicMethod) Proxy.newProxyInstance(
//                method1.getClass().getClassLoader(),
//                method1.getClass().getInterfaces(),
//                (proxy, method, args) -> {
//                    System.out.println("方法执行前");
//                    Object result = method.invoke(method1, args);
//                    System.out.println("方法执行后");
//                    return result;
//                }
//        );
//        method2.dynamic("什么意思!");
    }
}
