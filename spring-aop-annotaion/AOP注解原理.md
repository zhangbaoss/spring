# AOP注解启动流程 #
给配置类添加一个@EnableAspectJAutoProxy注解用来开启基于注解的Aop模式,现在来研究这个注解起到了什么作用

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @Import(AspectJAutoProxyRegistrar.class)
    public @interface EnableAspectJAutoProxy {

## 1.@EnableAspectJAutoProxy的作用: ##

使用@Import(AspectJAutoProxyRegistrar.class)注解给容器中导入一个AspectJAutoProxyRegistrar组件

    class AspectJAutoProxyRegistrar implements ImportBeanDefinitionRegistrar {

		@Override
		public void registerBeanDefinitions(
				AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
			
			//1.如果需要的话给容器中注册一个AspectJAnnotationAutoProxyCreator
			AopConfigUtils.registerAspectJAnnotationAutoProxyCreatorIfNecessary(registry);
		
			AnnotationAttributes enableAspectJAutoProxy =
					AnnotationConfigUtils.attributesFor(importingClassMetadata, EnableAspectJAutoProxy.class);
			if (enableAspectJAutoProxy.getBoolean("proxyTargetClass")) {
				AopConfigUtils.forceAutoProxyCreatorToUseClassProxying(registry);
			}
			if (enableAspectJAutoProxy.getBoolean("exposeProxy")) {
				AopConfigUtils.forceAutoProxyCreatorToExposeProxy(registry);
			}
		}
		
	}
AspectJAutoProxyRegistrar的作用:

给容器中注册一个AnnotationAwareAspectJAutoProxyCreator

    public static final String AUTO_PROXY_CREATOR_BEAN_NAME =
			"org.springframework.aop.config.internalAutoProxyCreator";

	public static BeanDefinition registerAspectJAnnotationAutoProxyCreatorIfNecessary(BeanDefinitionRegistry registry) {
		return registerAspectJAnnotationAutoProxyCreatorIfNecessary(registry, null);
	}

	public static BeanDefinition registerAspectJAnnotationAutoProxyCreatorIfNecessary(BeanDefinitionRegistry registry, Object source) {
		return registerOrEscalateApcAsRequired(AnnotationAwareAspectJAutoProxyCreator.class, registry, source);
	}

	/**
	 * @param cls 此时为AnnotationAwareAspectJAutoProxyCreator
	 */
    private static BeanDefinition registerOrEscalateApcAsRequired(Class<?> cls, BeanDefinitionRegistry registry, Object source) {
		Assert.notNull(registry, "BeanDefinitionRegistry must not be null");
		//首次进来registry中没有org.springframework.aop.config.internalAutoProxyCreator
		if (registry.containsBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME)) {
			BeanDefinition apcDefinition = registry.getBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME);
			if (!cls.getName().equals(apcDefinition.getBeanClassName())) {
				int currentPriority = findPriorityForClass(apcDefinition.getBeanClassName());
				int requiredPriority = findPriorityForClass(cls);
				if (currentPriority < requiredPriority) {
					apcDefinition.setBeanClassName(cls.getName());
				}
			}
			return null;
		}
		//将cls传入进去创建一个RootBeanDefinition
		RootBeanDefinition beanDefinition = new RootBeanDefinition(cls);
		beanDefinition.setSource(source);
		beanDefinition.getPropertyValues().add("order", Ordered.HIGHEST_PRECEDENCE);
		beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		//在容器中注册一个org.springframework.aop.config.internalAutoProxyCreator--AnnotationAwareAspectJAutoProxyCreator
		registry.registerBeanDefinition(AUTO_PROXY_CREATOR_BEAN_NAME, beanDefinition);
		return beanDefinition;
	}

最终使用@EnableAspectJAutoProxy注解给容器中注册了一个AnnotationAwareAspectJAutoProxyCreator组件

## 2.AnnotationAwareAspectJAutoProxyCreator有什么作用: ##

先看一下继承关系:

	==>表示继承类,-->表示实现接口
    AnnotationAwareAspectJAutoProxyCreator ==> AspectJAwareAdvisorAutoProxyCreator
    ==> AbstractAdvisorAutoProxyCreator ==> AbstractAutoProxyCreator -->SmartInstantiationAwareBeanPostProcessor, BeanFactoryAware

	其中
    SmartInstantiationAwareBeanPostProcessor --> InstantiationAwareBeanPostProcessor

	InstantiationAwareBeanPostProcessor和BeanPostProcessor两个后置处理器的不同看2.4.2.2.2)
可以看到AnnotationAwareAspectJAutoProxyCreator最终实现了SmartInstantiationAwareBeanPostProcessor, BeanFactoryAware两个接口
SmartInstantiationAwareBeanPostProcessor是一个后置处理器,在bean初始化前后执行.
BeanFactoryAware自动注入BeanFactory

此时我们需要关注哪些类里面重写了BeanPostProcessor和BeanFactoryAware中的方法:

	//此方法被AbstractAdvisorAutoProxyCreator类重写了
    AbstractAutoProxyCreator.setBeanFactory
	//两个后置处理器的方法
    AbstractAutoProxyCreator.postProcessBeforeInstantiation
    AbstractAutoProxyCreator.postProcessAfterInstantiation(没做操作)
	AbstractAutoProxyCreator.postProcessBeforeInitialization(没做操作)
	AbstractAutoProxyCreator.postProcessAfterInitialization

	//此时在调用setBeanFactory方法时调用了initBeanFactory方法
    AbstractAdvisorAutoProxyCreator.setBeanFactory-->initBeanFactory

	//重写了父类的initBeanFactory方法
    AnnotationAwareAspectJAutoProxyCreator.initBeanFactory

此时调用debug模式查看AnnotationAwareAspectJAutoProxyCreator做了什么
    
### 2.1).传入配置类,创建IOC容器 ###

    aopContext = new AnnotationConfigApplicationContext(AnnotationAopConfig.class);

### 2.2).注册配置类,调用refresh(); ###

    refresh();

### 2.3).注册bean的后置处理器来拦截bean的创建 ###

	registerBeanPostProcessors(beanFactory);

#### 2.3.1).先获取ioc容器中已经定义了的需要创建对象的所有BeanPostProcess ####

    String[] postProcessorNames = beanFactory.getBeanNamesForType(BeanPostProcessor.class, true, false);
    
#### 2.3.2).给容器中加别的BeanPostProcessor ####

    int beanProcessorTargetCount = beanFactory.getBeanPostProcessorCount() + 1 + postProcessorNames.length;
	beanFactory.addBeanPostProcessor(new BeanPostProcessorChecker(beanFactory, beanProcessorTargetCount));

#### 2.3.3).将BeanPostProcess根据实现的不同接口放入不同的集合中 ####

    List<BeanPostProcessor> priorityOrderedPostProcessors = new ArrayList<BeanPostProcessor>();
	List<BeanPostProcessor> internalPostProcessors = new ArrayList<BeanPostProcessor>();
	List<String> orderedPostProcessorNames = new ArrayList<String>();
	List<String> nonOrderedPostProcessorNames = new ArrayList<String>();
	for (String ppName : postProcessorNames) {
		if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
			BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
			priorityOrderedPostProcessors.add(pp);
			if (pp instanceof MergedBeanDefinitionPostProcessor) {
				internalPostProcessors.add(pp);
			}
		}
		else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
			orderedPostProcessorNames.add(ppName);
		}
		else {
			nonOrderedPostProcessorNames.add(ppName);
		}
	}

#### 2.3.4).优先注册实现了PriorityOrdered接口的BeanPostProcess ####
    
	sortPostProcessors(priorityOrderedPostProcessors, beanFactory);
	registerBeanPostProcessors(beanFactory, priorityOrderedPostProcessors);

#### 2.3.5).再注册实现了Ordered接口的BeanPostProcess ####

    List<BeanPostProcessor> orderedPostProcessors = new ArrayList<BeanPostProcessor>();
	for (String ppName : orderedPostProcessorNames) {
		BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
		orderedPostProcessors.add(pp);
		if (pp instanceof MergedBeanDefinitionPostProcessor) {
			internalPostProcessors.add(pp);
		}
	}
	sortPostProcessors(orderedPostProcessors, beanFactory);
	registerBeanPostProcessors(beanFactory, orderedPostProcessors);

#### 2.3.6).然后再注册没有实现优先级接口的BeanPostProcess ####

    List<BeanPostProcessor> nonOrderedPostProcessors = new ArrayList<BeanPostProcessor>();
	for (String ppName : nonOrderedPostProcessorNames) {
		BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
		nonOrderedPostProcessors.add(pp);
		if (pp instanceof MergedBeanDefinitionPostProcessor) {
			internalPostProcessors.add(pp);
		}
	}
	registerBeanPostProcessors(beanFactory, nonOrderedPostProcessors);

#### 2.3.7).最后再注册内部的BeanPostProcess ####

    sortPostProcessors(internalPostProcessors, beanFactory);
	registerBeanPostProcessors(beanFactory, internalPostProcessors);

#### 2.3.8).注册就是创建一个BeanPostProcess对象存放到容器中,以当前需要注册的名字为org.springframework.aop.config.internalAutoProxyCreator实际类型为AnnotationAwareAspectJAutoProxyCreator的BeanPostProcess为例,由于它实现了Ordered接口所以先获取Bean ####

	//此处ppName为org.springframework.aop.config.internalAutoProxyCreator
    BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);

    public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
		return doGetBean(name, requiredType, null, false);
	}
  
	//获取单实例bean
    sharedInstance = getSingleton(beanName, new ObjectFactory<Object>()

	//由于第一次获取,所以获取不到
    singletonObject = singletonFactory.getObject();
	
	//只能创建一个bean
    return createBean(beanName, mbd, args);

2.3.8.1).获取或创建Bean实例

    Object beanInstance = doCreateBean(beanName, mbdToUse, args);

    BeanWrapper instanceWrapper = null;
	if (mbd.isSingleton()) {
		instanceWrapper = this.factoryBeanInstanceCache.remove(beanName);
	}
	if (instanceWrapper == null) {
		instanceWrapper = createBeanInstance(beanName, mbd, args);
	}
	final Object bean = (instanceWrapper != null ? instanceWrapper.getWrappedInstance() : null);

2.3.8.2).执行populateBean方法给各种属性赋值

    populateBean(beanName, mbd, instanceWrapper);

2.3.8.3).执行initializeBean方法初始化Bean

    exposedObject = initializeBean(beanName, exposedObject, mbd);

2.3.8.3.1).执行invokeAwareMethods方法,由于AnnotationAwareAspectJAutoProxyCreator实现了BeanFactoryAware接口,此时会调用AbstractAdvisorAutoProxyCreator.setBeanFactory方法

    invokeAwareMethods(beanName, bean);

	//如果该类实现了BeanFactoryAware接口则会调用该类的setBeanFactory方法
    private void invokeAwareMethods(final String beanName, final Object bean) {
		if (bean instanceof Aware) {
			if (bean instanceof BeanNameAware) {
				((BeanNameAware) bean).setBeanName(beanName);
			}
			if (bean instanceof BeanClassLoaderAware) {
				((BeanClassLoaderAware) bean).setBeanClassLoader(getBeanClassLoader());
			}
			if (bean instanceof BeanFactoryAware) {
				((BeanFactoryAware) bean).setBeanFactory(AbstractAutowireCapableBeanFactory.this);
			}
		}
	}

2.3.8.3.2).执行applyBeanPostProcessorsBeforeInitialization方法,注册的就是AnnotationAwareAspectJAutoProxyCreator所以此时不会调用AbstractAutoProxyCreator后置处理器方法

    wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);

	//执行postProcessBeforeInitialization方法
    public Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String beanName)
			throws BeansException {

		Object result = existingBean;
		for (BeanPostProcessor beanProcessor : getBeanPostProcessors()) {
			result = beanProcessor.postProcessBeforeInitialization(result, beanName);
			if (result == null) {
				return result;
			}
		}
		return result;
	}

2.3.8.3.3).执行自定义的初始化方法

    invokeInitMethods(beanName, wrappedBean, mbd);

2.3.8.3.4).执行applyBeanPostProcessorsAfterInitialization方法,注册的就是AnnotationAwareAspectJAutoProxyCreator所以此时不会调用AbstractAutoProxyCreator后置处理器方法,还没注册好

    wrappedBean = applyBeanPostProcessorsAfterInitialization(wrappedBean, beanName);

    public Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String beanName)
			throws BeansException {

		Object result = existingBean;
		for (BeanPostProcessor beanProcessor : getBeanPostProcessors()) {
			result = beanProcessor.postProcessAfterInitialization(result, beanName);
			if (result == null) {
				return result;
			}
		}
		return result;
	}
	
2.3.8.4).BeanPostProcessor(AnnotationAwareAspectJAutoProxyCreator)创建成功

#### 2.3.9).将Bean注册到容器中 ####

    registerBeanPostProcessors(beanFactory, orderedPostProcessors);
	
    private static void registerBeanPostProcessors(
			ConfigurableListableBeanFactory beanFactory, List<BeanPostProcessor> postProcessors) {

		for (BeanPostProcessor postProcessor : postProcessors) {
			beanFactory.addBeanPostProcessor(postProcessor);
		}
	}

=============上面流程是AnnotationAwareAspectJAutoProxyCreator注册过程==============

### 2.4).实例化剩余的所有单实例Bean ###

    finishBeanFactoryInitialization(beanFactory);

    beanFactory.preInstantiateSingletons();

#### 2.4.1).遍历获取容器中所有的bean,依次创建对象 ####

    List<String> beanNames = new ArrayList<String>(this.beanDefinitionNames);

    getBean(beanName);

    return doGetBean(name, null, null, false);

#### 2.4.2).创建bean ####

2.4.2.1).先从缓存中获取当前bean，如果能获取到，说明bean是之前被创建过的，直接使用，否则再创建;只要创建好的Bean都会被缓存起来

	Object sharedInstance = getSingleton(beanName);

2.4.2.2).创建Bean

    sharedInstance = getSingleton(beanName, new ObjectFactory<Object>()

    singletonObject = singletonFactory.getObject();

    return createBean(beanName, mbd, args);

    Object bean = resolveBeforeInstantiation(beanName, mbdToUse);

2.4.2.2.1).AnnotationAwareAspectJAutoProxyCreator 会在任何bean创建之前先尝试返回bean的实例,resolveBeforeInstantiation(beanName, mbdToUse);解析BeforeInstantiation希望后置处理器在此能返回一个代理对象；

    protected Object resolveBeforeInstantiation(String beanName, RootBeanDefinition mbd) {
		Object bean = null;
		if (!Boolean.FALSE.equals(mbd.beforeInstantiationResolved)) {
			// Make sure bean class is actually resolved at this point.
			if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
				Class<?> targetType = determineTargetType(beanName, mbd);
				if (targetType != null) {
					//后置处理器先尝试返回对象
					bean = applyBeanPostProcessorsBeforeInstantiation(targetType, beanName);
					if (bean != null) {
						bean = applyBeanPostProcessorsAfterInitialization(bean, beanName);
					}
				}
			}
			mbd.beforeInstantiationResolved = (bean != null);
		}
		return bean;
	}

    拿到所有后置处理器，如果是InstantiationAwareBeanPostProcessor;就执行postProcessBeforeInstantiation
    protected Object applyBeanPostProcessorsBeforeInstantiation(Class<?> beanClass, String beanName) {
		for (BeanPostProcessor bp : getBeanPostProcessors()) {
			if (bp instanceof InstantiationAwareBeanPostProcessor) {
				InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
				Object result = ibp.postProcessBeforeInstantiation(beanClass, beanName);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

2.4.2.2.2).两个后置处理器的不同点在于:
    
    【BeanPostProcessor是在Bean对象创建完成初始化前后调用的】
	【InstantiationAwareBeanPostProcessor是在创建Bean实例之前先尝试用后置处理器返回对象的】

2.4.2.2.3).doCreateBean(beanName, mbdToUse, args);真正的去创建一个bean实例；和3.8.1流程一样；

## 3.AnnotationAwareAspectJAutoProxyCreator中InstantiationAwareBeanPostProcessor接口的作用 ##

每一个bean创建之前，调用postProcessBeforeInstantiation()；暂时只关注MathCalculator和AnnotationAspects的创建时该接口的作用

	//获取Bean名
	Object cacheKey = getCacheKey(beanClass, beanName);

	if (beanName == null || !this.targetSourcedBeans.contains(beanName)) {
		if (this.advisedBeans.containsKey(cacheKey)) {
			return null;
		}
		if (isInfrastructureClass(beanClass) || shouldSkip(beanClass, beanName)) {
			this.advisedBeans.put(cacheKey, Boolean.FALSE);
			return null;
		}
	}

### 3.1).获取Bean名 ###

	/**
	 * BeanFactory.FACTORY_BEAN_PREFIX="&"
	 * 如果一个Bean由BeanFactory创建,ioc获取BeanFactory实例时默认获取的是Bean的实例,
	 * 如果想获取BeanFactory实例需要在前面加上&符号
	 */
    protected Object getCacheKey(Class<?> beanClass, String beanName) {
		if (StringUtils.hasLength(beanName)) {
			return (FactoryBean.class.isAssignableFrom(beanClass) ?
					BeanFactory.FACTORY_BEAN_PREFIX + beanName : beanName);
		}
		else {
			return beanClass;
		}
	}

### 3.2).判断当前bean是否在advisedBeans中（保存了所有需要增强bean） ###

    if (this.advisedBeans.containsKey(cacheKey)) {
		return null;
	}

### 3.3).判断当前bean是否是基础类型的Advice、Pointcut、Advisor、AopInfrastructureBean，或者是否是切面（@Aspect） ###

    if (isInfrastructureClass(beanClass) || shouldSkip(beanClass, beanName)) {
		this.advisedBeans.put(cacheKey, Boolean.FALSE);
		return null;
	}

	//调用isInfrastructureClass判断是否是基础类型,isAspect是否是切面（@Aspect）
    protected boolean isInfrastructureClass(Class<?> beanClass) {
		return (super.isInfrastructureClass(beanClass) || this.aspectJAdvisorFactory.isAspect(beanClass));
	}

	//判断当前bean是否是基础类型的Advice、Pointcut、Advisor、AopInfrastructureBean
    protected boolean isInfrastructureClass(Class<?> beanClass) {
		boolean retVal = Advice.class.isAssignableFrom(beanClass) ||
				Pointcut.class.isAssignableFrom(beanClass) ||
				Advisor.class.isAssignableFrom(beanClass) ||
				AopInfrastructureBean.class.isAssignableFrom(beanClass);
		if (retVal && logger.isTraceEnabled()) {
			logger.trace("Did not attempt to auto-proxy infrastructure class [" + beanClass.getName() + "]");
		}
		return retVal;
	}

### 3.4).判断是否需要跳过 ###

    protected boolean shouldSkip(Class<?> beanClass, String beanName) {
		//获取候选的增强器（切面里面的通知方法）【List<Advisor> candidateAdvisors】
		//每一个封装的通知方法的增强器是 InstantiationModelAwarePointcutAdvisor；
		//判断每一个增强器是否是 AspectJPointcutAdvisor 类型的
		List<Advisor> candidateAdvisors = findCandidateAdvisors();
		for (Advisor advisor : candidateAdvisors) {
			if (advisor instanceof AspectJPointcutAdvisor) {
				if (((AbstractAspectJAdvice) advisor.getAdvice()).getAspectName().equals(beanName)) {
					return true;
				}
			}
		}
		//否则return false
		return super.shouldSkip(beanClass, beanName);
	}

    protected boolean shouldSkip(Class<?> beanClass, String beanName) {
		return false;
	}

## 4.AnnotationAwareAspectJAutoProxyCreator中BeanPostProcessor接口的作用 ##
每一个bean初始化之后，调用postProcessAfterInitialization()；

	return wrapIfNecessary(bean, beanName, cacheKey);

### 4.1).获取当前bean的所有增强器（通知方法） ###

    Object[] specificInterceptors = getAdvicesAndAdvisorsForBean(bean.getClass(), beanName, null);

    protected Object[] getAdvicesAndAdvisorsForBean(Class<?> beanClass, String beanName, TargetSource targetSource) {
		List<Advisor> advisors = findEligibleAdvisors(beanClass, beanName);
		if (advisors.isEmpty()) {
			return DO_NOT_PROXY;
		}
		return advisors.toArray();
	}

    protected List<Advisor> findEligibleAdvisors(Class<?> beanClass, String beanName) {
		//找到候选的所有的增强器
		List<Advisor> candidateAdvisors = findCandidateAdvisors();
		//获取到能在bean使用的增强器（找哪些通知方法是需要切入当前bean方法的）
		List<Advisor> eligibleAdvisors = findAdvisorsThatCanApply(candidateAdvisors, beanClass, beanName);
		extendAdvisors(eligibleAdvisors);
		if (!eligibleAdvisors.isEmpty()) {
			//给增强器排序
			eligibleAdvisors = sortAdvisors(eligibleAdvisors);
		}
		return eligibleAdvisors;
	}

### 4.2).判断当前Bean是否需要增强,如果不需要增强直接将当前bean保存在advisedBeans中 ###

    this.advisedBeans.put(cacheKey, Boolean.FALSE);

### 4.3).如果需要增强,创建当前Bean的代理对象 ###

    Object proxy = createProxy(
			bean.getClass(), beanName, specificInterceptors, new SingletonTargetSource(bean));

4.3.1).获取所有的增强器(通知方法)

    Advisor[] advisors = buildAdvisors(beanName, specificInterceptors);

4.3.2).保存到proxyFactory

    proxyFactory.addAdvisors(advisors);

4.3.3).创建代理对象：Spring自动决定

    return proxyFactory.getProxy(getProxyClassLoader());

    public Object getProxy(ClassLoader classLoader) {
		return createAopProxy().getProxy(classLoader);
	}
    
    protected final synchronized AopProxy createAopProxy() {
		if (!this.active) {
			activate();
		}
		return getAopProxyFactory().createAopProxy(this);
	}

    public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {
		if (config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config)) {
			Class<?> targetClass = config.getTargetClass();
			if (targetClass == null) {
				throw new AopConfigException("TargetSource cannot determine target class: " +
						"Either an interface or a target is required for proxy creation.");
			}
			if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
				return new JdkDynamicAopProxy(config);
			}
			return new ObjenesisCglibAopProxy(config);
		}
		else {
			return new JdkDynamicAopProxy(config);
		}
	}

### 4.4).给容器返回当前组件的代理对象,以后容器中获取到的就是这个组件的代理对象，执行目标方法的时候，代理对象就会执行通知方法的流程; ###

## 5.目标方法的执行 ##

    calculator.div(1, 1);
此时calculator是容器中保存的代理对象（cglib增强后的对象），这个对象里面保存了详细信息（比如增强器，目标对象，xxx）

### 5.1).拦截目标方法的执行 ###

    CglibAopProxy.intercept();

### 5.2).根据BeanFactory对象获取将要执行的目标方法拦截器链,拦截器链（每一个通知方法又被包装为方法拦截器，利用MethodInterceptor机制） ###

    List<Object> chain = this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, targetClass);

    public List<Object> getInterceptorsAndDynamicInterceptionAdvice(Method method, Class<?> targetClass) {
		MethodCacheKey cacheKey = new MethodCacheKey(method);
		List<Object> cached = this.methodCache.get(cacheKey);
		if (cached == null) {
			cached = this.advisorChainFactory.getInterceptorsAndDynamicInterceptionAdvice(
					this, method, targetClass);
			this.methodCache.put(cacheKey, cached);
		}
		return cached;
	}

5.2.1).保存所有的拦截器

    List<Object> interceptorList

此时集合的长度为5,表示一个默认的ExposeInvocationInterceptor 和 4个增强器

5.2.2).遍历循环所有的增强器,将其转换为MethodInterceptor[]

    MethodInterceptor[] interceptors = registry.getInterceptors(advisor);

    public MethodInterceptor[] getInterceptors(Advisor advisor) throws UnknownAdviceTypeException {
		List<MethodInterceptor> interceptors = new ArrayList<MethodInterceptor>(3);
		Advice advice = advisor.getAdvice();
		//增强器如果是MethodInterceptor类型则放入数组中
		if (advice instanceof MethodInterceptor) {
			interceptors.add((MethodInterceptor) advice);
		}
		//遍历所有的装饰器,将增强器转为MethodInterceptor[]类型
		for (AdvisorAdapter adapter : this.adapters) {
			if (adapter.supportsAdvice(advice)) {
				interceptors.add(adapter.getInterceptor(advisor));
			}
		}
		if (interceptors.isEmpty()) {
			throw new UnknownAdviceTypeException(advisor.getAdvice());
		}
		return interceptors.toArray(new MethodInterceptor[interceptors.size()]);
	}

5.2.3).返回拦截器集合

### 5.3).如果没有拦截器链,直接执行目标方法 ###

    retVal = methodProxy.invoke(target, argsToUse);

### 5.4).如果有拦截器链,将需要执行的目标对象,目标方法,拦截器链等信息传入创建一个CglibMethodInvocation对象,并调用proceed()方法 ###

    retVal = new CglibMethodInvocation(proxy, target, method, args, targetClass, chain, methodProxy).proceed();

5.4.1).proceed方法中通知方法执行顺序

    public Object proceed() throws Throwable {
		//currentInterceptorIndex:当前执行到的拦截器下标
		//interceptorsAndDynamicMethodMatchers:5.2.3返回的拦截器数组
		//当且仅当拦截器数组中没有拦截器时或执行到拦截器数组中最后一个拦截器时才执行invokeJoinpoint方法
		if (this.currentInterceptorIndex == this.interceptorsAndDynamicMethodMatchers.size() - 1) {
			return invokeJoinpoint();
		}

		//获取拦截器
		Object interceptorOrInterceptionAdvice =
				this.interceptorsAndDynamicMethodMatchers.get(++this.currentInterceptorIndex);
		return ((MethodInterceptor) interceptorOrInterceptionAdvice).invoke(this);
	}
以本文的5.2.1中获取的5个拦截器为例

* ExposeInvocationInterceptor
* AspectJAfterThrowingAdvice
* AspectJAfterReturningAdvice
* AspectJAfterAdvice
* AspectJMethodBeforeAdvice

①.首次进入proceed方法,此时currentInterceptorIndex=-1,size-1=4,所以不会执行invokeJoinpoint方法,currentInterceptorIndex自增变为0,获取到第一个拦截器ExposeInvocationInterceptor,接着执行invoke(this)方法
    
    public Object invoke(MethodInvocation mi) throws Throwable {
		//从线程缓存中获取旧的MethodInvocation
		MethodInvocation oldInvocation = invocation.get();
		//将新的MethodInvocation设置到线程缓存中
		invocation.set(mi);
		try {
			//执行proceed方法
			return mi.proceed();
		}
		finally {
			invocation.set(oldInvocation);
		}
	}

②.此时第一个拦截器会执行proceed方法,回到了5.4.1的方法中,此时currentInterceptorIndex=0,size-1=4,所以不会执行invokeJoinpoint方法,currentInterceptorIndex自增变为1,获取到第二个拦截器AspectJAfterThrowingAdvice,接着执行invoke(this)方法

    public Object invoke(MethodInvocation mi) throws Throwable {
		try {
			return mi.proceed();
		}
		catch (Throwable ex) {
			if (shouldInvokeOnThrowing(ex)) {
				invokeAdviceMethod(getJoinPointMatch(), null, ex);
			}
			throw ex;
		}
	}

③.此时第二个拦截器会执行proceed方法,回到了5.4.1的方法中,此时currentInterceptorIndex=1,size-1=4,所以不会执行invokeJoinpoint方法,currentInterceptorIndex自增变为2,获取到第三个拦截器AspectJAfterReturningAdvice,接着执行invoke(this)方法

    public Object invoke(MethodInvocation mi) throws Throwable {
		Object retVal = mi.proceed();
		this.advice.afterReturning(retVal, mi.getMethod(), mi.getArguments(), mi.getThis());
		return retVal;
	}

④.此时第三个拦截器会执行proceed方法,回到了5.4.1的方法中,此时currentInterceptorIndex=2,size-1=4,所以不会执行invokeJoinpoint方法,currentInterceptorIndex自增变为3,获取到第四个拦截器AspectJAfterAdvice,接着执行invoke(this)方法

    public Object invoke(MethodInvocation mi) throws Throwable {
		try {
			return mi.proceed();
		}
		finally {
			invokeAdviceMethod(getJoinPointMatch(), null, null);
		}
	}

⑤.此时第四个拦截器会执行proceed方法,回到了5.4.1的方法中,此时currentInterceptorIndex=3,size-1=4,所以不会执行invokeJoinpoint方法,currentInterceptorIndex自增变为4,获取到第五个拦截器AspectJMethodBeforeAdvice,接着执行invoke(this)方法

    public Object invoke(MethodInvocation mi) throws Throwable {
		this.advice.before(mi.getMethod(), mi.getArguments(), mi.getThis() );
		return mi.proceed();
	}

⑥.第五个拦截器先执行before方法,打印前置通知,然后接着调用5.4.1的方法,此时由于currentInterceptorIndex=size-1=4,所以执行invokeJoinpoint方法

    protected Object invokeJoinpoint() throws Throwable {
		if (this.publicMethod) {
			//调用执行方法
			return this.methodProxy.invoke(this.target, this.arguments);
		}
		else {
			return super.invokeJoinpoint();
		}
	}

⑦.由于有finally语句块,所以方法调用成功和失败都返回到步骤④,继续执行后面方法

    invokeAdviceMethod(getJoinPointMatch(), null, null);

⑧.如果方法调用成功返回步骤③,执行后面方法

    this.advice.afterReturning(retVal, mi.getMethod(), mi.getArguments(), mi.getThis());

⑨.如果方法执行期间出现异常则回到步骤②,执行异常方法

    invokeAdviceMethod(getJoinPointMatch(), null, ex);

⑩.到此aop通知执行完成,顺序是:

* 先执行前置通知
* 执行目标方法
* 执行后置通知
* 如果没有异常执行返回通知
* 如果出现异常执行异常通知

5.4.2).拦截器链的触发过程:

 * 1)、如果没有拦截器执行执行目标方法，或者拦截器的索引和拦截器数组-1大小一样（指定到了最后一个拦截器）执行目标方法；
 * 2)、链式获取每一个拦截器，拦截器执行invoke方法，每一个拦截器等待下一个拦截器执行完成返回以后再来执行；

5.4.3).拦截器链的作用:

拦截器链的机制，保证通知方法与目标方法的执行顺序；


## 总结 ##

* @EnableAspectJAutoProxy开启AOP功能
* @EnableAspectJAutoProxy会给容器注册一个组件AnnotationAwareAspectJAutoProxyCreator
* AnnotationAwareAspectJAutoProxyCreator是一个后置处理器
* registerBeanPostProcessors()注册后置处理器;创建AnnotationAwareAspectJAutoProxyCreator对象
* finishBeanFactoryInitialization（）初始化剩下的单实例bean
	* 创建业务逻辑组件和切面组件
	* AnnotationAwareAspectJAutoProxyCreator拦截组件的创建过程
	* 组件创建完之后，判断组件是否需要增强
* 代理对象执行目标方法
* CglibAopProxy.intercept()；
	* 得到目标方法的拦截器链（增强器包装成拦截器MethodInterceptor）
	* 利用拦截器的链式机制，依次进入每一个拦截器进行执行；   


