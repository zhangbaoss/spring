package com.zhangbao.mapper;

import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/25 17:46
 * @Version 1.0
 **/
public interface EmployeeMapper {

    /**
     * 查询所有员工
     * @return
     */
    @Select("SELECT * FROM employee;")
    List<Map<String, Object>> list();
}
