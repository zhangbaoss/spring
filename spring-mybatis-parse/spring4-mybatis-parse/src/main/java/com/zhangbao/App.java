package com.zhangbao;

import com.zhangbao.config.MybatisConfig;
import com.zhangbao.mapper.EmployeeMapper;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/25 21:32
 * @Version 1.0
 **/
public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(MybatisConfig.class);
        EmployeeMapper employeeMapper = context.getBean(EmployeeMapper.class);
        System.out.println(employeeMapper.list());
    }
}
