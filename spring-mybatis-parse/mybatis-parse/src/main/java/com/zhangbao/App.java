package com.zhangbao;

import com.zhangbao.mapper.EmployeeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/25 17:31
 * @Version 1.0
 **/
public class App {
    public static void main(String[] args) {
        String resource = "mybatis-config.xml";
        try (InputStream inputStream = Resources.getResourceAsStream(resource)) {
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
            SqlSession session = sqlSessionFactory.openSession();
            // mybatis-config.xml文件中的mappers标签可以用下面代码代替
            session.getConfiguration().addMapper(EmployeeMapper.class);
            EmployeeMapper mapper = session.getMapper(EmployeeMapper.class);
            System.out.println(mapper.list());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
