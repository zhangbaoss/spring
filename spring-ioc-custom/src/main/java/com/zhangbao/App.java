package com.zhangbao;

import com.zhangbao.config.AppConfig;
import com.zhangbao.controller.SpringIocAnnotationController;
import com.zhangbao.factory.ExtAnnotationConfigApplicationContext;

/**
 * 程序入口类
 * @author zhangbao
 * @date 2020-11-09
 */
public class App {

    public static void main( String[] args) {
        ExtAnnotationConfigApplicationContext app =
                new ExtAnnotationConfigApplicationContext(AppConfig.class);
        SpringIocAnnotationController springIocAnnotationController =
                (SpringIocAnnotationController) app.getBean("springIocAnnotationController");
        springIocAnnotationController.test();
    }

}
