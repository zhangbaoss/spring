package com.zhangbao.reader;

import com.zhangbao.annotation.*;
import com.zhangbao.constants.ScopeModel;
import com.zhangbao.definition.BeanDefinition;
import com.zhangbao.register.BeanDefinitionRegister;

import java.util.List;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2020/11/10 10:03
 * @Version 1.0
 **/
public class BeanDefinitionReader {

    private final BeanDefinitionRegister register;

    public BeanDefinitionReader(BeanDefinitionRegister register) {
        this.register = register;
    }

    public void register(List<String> classNames){
        for (String className : classNames) {
            register(className);
        }
    }

    private void register(String className) {
        try {
            Class<?> clazz = Class.forName(className);
            if (findAnnotation(clazz)) {
                registerBean(clazz, className);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean findAnnotation(Class<?> clazz) {
        if (clazz.isAnnotationPresent(ExtController.class)) {
            return true;
        }
        if (clazz.isAnnotationPresent(ExtService.class)) {
            return true;
        }
        if (clazz.isAnnotationPresent(ExtRepository.class)) {
            return true;
        }
        return clazz.isAnnotationPresent(ExtComponent.class);
    }

    private void registerBean(Class<?> clazz, String className) {
        final BeanDefinition bd = new BeanDefinition();
        bd.setClassName(clazz.getSimpleName());
        bd.setClazz(clazz);
        bd.setScope(ScopeModel.SINGLETON);
        if (clazz.isAnnotationPresent(ExtScope.class)) {
            bd.setScope(clazz.getAnnotation(ExtScope.class).value());
        }
        if (clazz.isAnnotationPresent(ExtLazy.class)) {
            bd.setLazy(clazz.getAnnotation(ExtLazy.class).value());
        }
        register.registerBeanDefinition(getBeanName(clazz, className), bd);
    }

    private String getBeanName(Class<?> clazz, String className) {
        String beanName = "";
        if (clazz.isAnnotationPresent(ExtController.class)) {
            beanName = clazz.getAnnotation(ExtController.class).value();
        }
        if (clazz.isAnnotationPresent(ExtService.class)) {
            beanName = clazz.getAnnotation(ExtService.class).value();
        }
        if (clazz.isAnnotationPresent(ExtRepository.class)) {
            beanName = clazz.getAnnotation(ExtRepository.class).value();
        }
        if (clazz.isAnnotationPresent(ExtComponent.class)) {
            beanName = clazz.getAnnotation(ExtComponent.class).value();
        }
        return "".equals(beanName) ?
                toLowerCaseFirstOne(className.substring(className.lastIndexOf(".") + 1)) : beanName;
    }

    private static String toLowerCaseFirstOne(String str){
        if (str == null || str.length() <= 0) {
            throw new RuntimeException("字符串有问题!");
        }
        if(Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }
}
