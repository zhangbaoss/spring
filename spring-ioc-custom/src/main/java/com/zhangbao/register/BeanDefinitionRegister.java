package com.zhangbao.register;

import com.zhangbao.definition.BeanDefinition;

import java.util.List;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2020/11/10 10:06
 * @Version 1.0
 **/
public interface BeanDefinitionRegister {

    /**
     * 注册类名
     * @param classNames 类名
     */
    void registerClassNames(List<String> classNames);

    /**
     * 注册beanDefinition
     * @param beanName bean名称
     * @param beanDefinition bean定义
     */
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition);
}
