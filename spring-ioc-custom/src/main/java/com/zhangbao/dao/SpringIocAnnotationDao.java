package com.zhangbao.dao;

import com.zhangbao.annotation.ExtAutowired;
import com.zhangbao.annotation.ExtRepository;
import com.zhangbao.service.SpringIocAnnotationService;

/**
 *
 * @author zhangbao
 * @date 2020-11-09
 */
@ExtRepository
public class SpringIocAnnotationDao {

	@ExtAutowired
	private SpringIocAnnotationService springIocAnnotationService;

	public void test() {
		System.out.println("到达dao层");
	}
}
