package com.zhangbao.definition;

import com.zhangbao.constants.ScopeModel;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2020/11/10 9:29
 * @Version 1.0
 **/
public class BeanDefinition {
    /**类名*/
    private String className;
    /**类的类型*/
    private Class<?> clazz;
    /**作用域*/
    private ScopeModel scope;
    private boolean lazy;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public ScopeModel getScope() {
        return scope;
    }

    public void setScope(ScopeModel scope) {
        this.scope = scope;
    }

    public boolean isLazy() {
        return lazy;
    }

    public void setLazy(boolean lazy) {
        this.lazy = lazy;
    }
}
