package com.zhangbao.scan;

import com.zhangbao.annotation.ExtComponentScan;
import com.zhangbao.register.BeanDefinitionRegister;
import com.zhangbao.utils.ClassUtils;

import java.util.List;

/**
 * @Description 扫描器,扫描配置类下的所有加了注解的文件
 * @Author zhangbao
 * @Date 2020/11/10 10:29
 * @Version 1.0
 **/
public class BeanDefinitionScan {

    private final BeanDefinitionRegister register;

    public BeanDefinitionScan(BeanDefinitionRegister register) {
        this.register = register;
    }

    public void doScan(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(ExtComponentScan.class)) {
            throw new RuntimeException("没有指定扫描的包!");
        }
        // 获取加了该注解的配置类所在的包目录
        String packageName = clazz.getAnnotation(ExtComponentScan.class).value();
        if ("".equals(packageName)) {
            throw new RuntimeException("没有指定扫描的包!");
        }
        try {
            // 获取当下包中所有类
            List<String> classes = ClassUtils.getClassesFormPackage(packageName);
            if (classes == null || classes.isEmpty()) {
                throw new RuntimeException("该包下没有任何类");
            }
            register.registerClassNames(classes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
