package com.zhangbao.config;

import com.zhangbao.annotation.ExtComponentScan;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2020-11-09
 * @Version 1.0
 **/
@ExtComponentScan("com.zhangbao")
public class AppConfig {
}
