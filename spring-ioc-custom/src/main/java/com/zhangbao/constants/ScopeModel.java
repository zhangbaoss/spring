package com.zhangbao.constants;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2020/11/10 11:38
 * @Version 1.0
 **/
public enum ScopeModel {
    /**单例*/
    SINGLETON,
    /**原型*/
    PROTOTYPE;
}
