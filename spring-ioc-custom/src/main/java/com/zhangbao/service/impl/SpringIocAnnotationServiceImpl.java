package com.zhangbao.service.impl;


import com.zhangbao.annotation.ExtAutowired;
import com.zhangbao.annotation.ExtService;
import com.zhangbao.dao.SpringIocAnnotationDao;
import com.zhangbao.service.SpringIocAnnotationService;


/**
 * @author zhangbao
 * @date 2020-11-09
 */
@ExtService("springIocAnnotationService")
public class SpringIocAnnotationServiceImpl implements SpringIocAnnotationService {

	private String abc = "123";
	private String efg;

	@ExtAutowired
	private SpringIocAnnotationDao springIocAnnotationDao;

	@Override
	public void test() {
		System.out.println("到达service层");
		springIocAnnotationDao.test();
	}
}
