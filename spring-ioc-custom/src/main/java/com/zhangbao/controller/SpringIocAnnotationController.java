package com.zhangbao.controller;

import com.zhangbao.annotation.ExtAutowired;
import com.zhangbao.annotation.ExtController;
import com.zhangbao.service.SpringIocAnnotationService;

/**
 * @author zhangbao
 * @date 2020-11-09
 */
@ExtController
public class SpringIocAnnotationController {

	@ExtAutowired
	private SpringIocAnnotationService springIocAnnotationService;

	public void test() {
		System.out.println("到达controller层");
		springIocAnnotationService.test();
	}
}
