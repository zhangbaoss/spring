package com.zhangbao.factory;

/**
 * bean工厂
 * @author zhangbao
 * @date 2020-11-09
 */
public interface BeanFactory {

	/**
	 * 对FactoryBean的转义定义,因为如果使用bean的名字检索FactoryBean得到的对象
	 * 是工厂生成的对象,如果需要得到工厂本身,需要转义
	 */
	String FACTORY_BEAN_PREFIX = "&";

	/**
	 * 根据bean的名字,获取在IOC容器中得到的bean实例
	 * @param name bean的名字
	 * @return bean实例
	 */
	Object getBean(String name);

	/**
	 * 根据bean名字得到bean实例,同时判断这个bean是不是单例
	 * @param name bean名字
	 * @return true:单例; false:不是单例
	 */
	boolean isSingleton(String name);

}
