package com.zhangbao.factory;

import com.zhangbao.annotation.ExtAutowired;
import com.zhangbao.constants.ScopeModel;
import com.zhangbao.definition.BeanDefinition;
import com.zhangbao.reader.BeanDefinitionReader;
import com.zhangbao.register.BeanDefinitionRegister;
import com.zhangbao.scan.BeanDefinitionScan;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 〈一句话功能简述〉<br>
 * 〈ioc容器〉
 *
 * @author zhangbao
 * @date 2020-11-09
 * @since 1.0.0
 */
public class ExtAnnotationConfigApplicationContext implements BeanDefinitionRegister, BeanFactory {

	private final BeanDefinitionScan scan;

	private final BeanDefinitionReader reader;

	private List<String> classNames;

	/**用来存放已经构建好的单例对象*/
	private final ConcurrentHashMap<String, Object> singletonObjects = new ConcurrentHashMap<>(16);

	/**用来存放正在创建的单例对象*/
	private final ConcurrentHashMap<String, Object> earlySingletonObjects = new ConcurrentHashMap<>(16);

	/**用来存放正在创建的单例对象名称*/
	private final Set<String> singletonsCurrentlyInCreation = Collections.newSetFromMap(new ConcurrentHashMap<>(256));

	/**用来存放beanName和BeanDefinition*/
	private final ConcurrentHashMap<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>(16);

	public ExtAnnotationConfigApplicationContext() {
		this.scan = new BeanDefinitionScan(this);
		this.reader = new BeanDefinitionReader(this);
	}

	public ExtAnnotationConfigApplicationContext(Class<?> clazz) {
		this();
		scan(clazz);
		register();
		refresh();
	}

	private void scan(Class<?> clazz) {
		this.scan.doScan(clazz);
	}

	public void register() {
		this.reader.register(classNames);
	}

	private void refresh() {
		for (Map.Entry<String, BeanDefinition> entry : beanDefinitionMap.entrySet()) {
			final BeanDefinition beanDefinition = entry.getValue();
			if (beanDefinition.isLazy()) {
				return;
			}
			getBean(entry.getKey());
		}
	}

	@Override
	public Object getBean(String beanName) {
		if (!isSingleton(beanName)) {
			return doCreateBean(beanName);
		}
		final Object bean = getSingleton(beanName);
		if (bean != null) {
			return bean;
		}
		return createBean(beanName);
	}

	@Override
	public boolean isSingleton(String name) {
		return ScopeModel.SINGLETON == beanDefinitionMap.get(name).getScope();
	}

	@Override
	public void registerClassNames(List<String> classNames) {
		this.classNames = classNames;
	}

	@Override
	public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) {
		beanDefinitionMap.put(beanName, beanDefinition);
	}

	private boolean isSingletonCurrentlyInCreation(String beanName) {
		return singletonsCurrentlyInCreation.contains(beanName);
	}

	private Object getSingleton(String beanName) {
		if (beanName == null) {
			throw new RuntimeException("beanName不能为空");
		}
		Object singletonObject = this.singletonObjects.get(beanName);
		if (singletonObject == null && isSingletonCurrentlyInCreation(beanName)) {
			singletonObject = this.earlySingletonObjects.get(beanName);
		}
		return singletonObject;
	}

	private Object createBean(String beanName) {
		this.singletonsCurrentlyInCreation.add(beanName);
		final BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
		final Class<?> clazz = beanDefinition.getClazz();
		try {
			final Object bean = doCreateBean(beanName);
			this.earlySingletonObjects.put(beanName, bean);
			populateBean(clazz, bean);
			this.singletonObjects.put(beanName, bean);
			this.singletonsCurrentlyInCreation.remove(beanName);
			this.earlySingletonObjects.remove(beanName);
			return bean;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException("属性填充失败!");
		}
	}


	private Object doCreateBean(String beanName) {
		final BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
		final Class<?> clazz = beanDefinition.getClazz();
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException("创建对象失败!");
		}
	}


	private void populateBean(Class<?> clazz, Object bean) throws IllegalAccessException {
		final Field[] fields = clazz.getDeclaredFields();
		if (fields.length <= 0) {
			return;
		}
		for (Field field : fields) {
			field.setAccessible(true);
			if (!field.isAnnotationPresent(ExtAutowired.class)) {
				continue;
			}
			field.set(bean, getBean(field.getName()));
		}
	}
}
