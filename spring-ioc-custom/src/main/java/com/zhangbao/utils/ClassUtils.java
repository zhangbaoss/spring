package com.zhangbao.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈Class类工具类〉
 *
 * @author a1638
 * @create 2019/1/12
 * @since 1.0.0
 */
public class ClassUtils {

	private static List<String> classes;

	private static final String CLASS_SUFFIX = ".class";

	static {
		classes = new ArrayList<>();
	}

	public static List<String> getClassesFormPackage(String packageName) throws Exception {
		//1.判断包名
		if (packageName == null || "".equals(packageName)) {
			throw new Exception("获取需要扫描的包失败!");
		}
		//2.获取扫描包路径
		String packagePath = getPackagePath(packageName);

		//3.获取该包下所有类,放入集合内
		getClassForPath(new File(packagePath), getPath());
		return classes;
	}

	/**
	 * 获取项目路径
	 * @return
	 */
	private static String getPath() throws UnsupportedEncodingException {
		String classPath = ClassUtils.class.getResource("/").getPath();
		//解决path乱码
		classPath = URLDecoder.decode(classPath, "utf-8");
		return classPath;
	}

	/**
	 * 获取扫描包路径
	 * @return
	 * @param packageName
	 */
	private static String getPackagePath(String packageName) throws UnsupportedEncodingException {
		//1.获取项目类路径
		String classPath = getPath();
		//2.将包名转换为类名
		String packagePath = packageName.replace(".", File.separator);
		//将项目路径和包路径合在一起
		packagePath = classPath + packagePath;
		return packagePath;
	}

	/**
	 * 获取该包下所有类,放入集合内
	 * @param packageFile
	 */
	private static void getClassForPath(File packageFile, String packagePath) throws ClassNotFoundException {
		if (packageFile.isDirectory()) {
			File[] files = packageFile.listFiles();
			if (files == null) {
				return;
			}
			for (File file : files) {
				getClassForPath(file, packagePath);
			}
		} else {
			String classPath = packageFile.getPath();
			if (packageFile.getName().endsWith(CLASS_SUFFIX)) {
				String className = classPath.replace(packagePath.replace("/","\\")
						.replaceFirst("\\\\",""),"").replace("\\",".")
						.replace(".class","");
				classes.add(className);
			}
		}
	}
}
