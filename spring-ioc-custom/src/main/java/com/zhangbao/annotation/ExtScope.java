package com.zhangbao.annotation;

import com.zhangbao.constants.ScopeModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2020/11/10 11:23
 * @Version 1.0
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExtScope {

    ScopeModel value() default ScopeModel.SINGLETON;
}
