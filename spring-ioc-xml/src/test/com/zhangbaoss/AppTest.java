package com.zhangbaoss;

import com.zhangbaoss.ioc.ExtClassPathXmlApplicationContext;
import com.zhangbaoss.service.SpringIocXmlService;
import org.junit.Test;

/**
 * 〈一句话功能简述〉<br>
 * 〈测试类〉
 *
 * @author a1638
 * @create 2019/1/11
 * @since 1.0.0
 */
public class AppTest {

	@Test
	public void test1() throws Exception {
		ExtClassPathXmlApplicationContext context =
				new ExtClassPathXmlApplicationContext("applicationContext.xml");
		SpringIocXmlService springIocXmlService = (SpringIocXmlService) context.getBean("springIocXmlService");
		System.out.println(springIocXmlService);
	}
}
