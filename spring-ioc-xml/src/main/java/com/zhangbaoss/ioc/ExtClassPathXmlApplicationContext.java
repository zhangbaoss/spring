package com.zhangbaoss.ioc;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.util.StringUtils;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈ioc容器〉
 *
 * @author a1638
 * @create 2019/1/11
 * @since 1.0.0
 */
public class ExtClassPathXmlApplicationContext {

	/**此处key是标签的id,value是class属性所在的对象*/
	private static Map<String, Object> map = new HashMap<>();

	public ExtClassPathXmlApplicationContext(String path) throws Exception {
		initContext(path);
	}

	/**
	 * 初始化容器
	 * @param path
	 * @throws Exception
	 */
	private void initContext(String path) throws Exception {
		//解析xml文件
		SAXReader reader = new SAXReader();
		Document document = reader.read(getResourceAsStream(path));
		//获取根节点
		Element rootElement = document.getRootElement();
		// 第一层标签(类信息)
		for (Iterator<Element> iteratorFirst = rootElement.elementIterator(); iteratorFirst.hasNext();) {
			Element firstChildNode = iteratorFirst.next();
			// 获取id名字
			String idName = firstChildNode.attributeValue("id");
			// 获取class名字
			String className = firstChildNode.attributeValue("class");
			// 获取该标签类信息
			Class<?> clazz = Class.forName(className);

			// 构造对象
			Object object = null;

			// 第二层标签(属性信息)
			for (Iterator<Element> iteratorSecond = firstChildNode.elementIterator(); iteratorSecond.hasNext();) {
				Element secondChildNode = iteratorSecond.next();
				// 如果是属性
				if ("property".equals(secondChildNode.getName())) {
					// 1.由于是属性注入,所以没有特殊的构造方法,所以object可以直接获取
					object = clazz.newInstance();
					// 2.获取ref的值,根据ref的值从map中获取需要注入的对象,这里的ref对应的是bean标签的id值
					String refValue = secondChildNode.attributeValue("ref");
					Object injectObj = map.get(refValue);
					if (injectObj == null) {
						throw new Exception("简易版本没有解决循环依赖,在xml中请按照依赖顺序书写bean标签,被依赖的bean放到上面!");
					}
					// 3.由于是属性,所以需要获取name的值,这里的name对应的是对象中的set方法名,即如果name=service,则方法名应为setService
					String nameValue = secondChildNode.attributeValue("name");
					// 4.从已获取的对象中找到该属性的set方法进行注入
					Field field = clazz.getDeclaredField(nameValue);
					// 5.由于属性是私有的,所以需要设置权限
					field.setAccessible(true);
					// 6.注入属性
					field.set(object, injectObj);
				} else if ("constructor-arg".equals(secondChildNode.getName())) {
					// 如果是构造函数,说明有特殊的构造方法
					// 1.获取子标签的name
					String nameValue = secondChildNode.attributeValue("name");
					// 2.根据name从被注入对象中获取该属性
					Field field = clazz.getDeclaredField(nameValue);
					// 3.根据属性获取注入的对象的类信息,用来获取被注入对象的特殊构造方法
					Class<?> fieldType = field.getType();
					// 4.获取被注入对象的特殊构造方法
					Constructor<?> constructor = clazz.getConstructor(fieldType);
					// 5.根据ref的值从map中获取被注入的对象
					String refValue = secondChildNode.attributeValue("ref");
					Object injectObj = map.get(refValue);
					if (injectObj == null) {
						throw new Exception("简易版本没有解决循环依赖,请按照依赖顺序书写bean标签,被依赖的bean放到上面!");
					}
					// 6.根据特殊构造方法,构造对象
					object = constructor.newInstance(injectObj);
				} else {
					throw new Exception("没有这个标签!错误标签==>" + secondChildNode.getName());
				}
			}

			if (object == null) {
				// 如果没有子标签(没有属性信息)
				object = clazz.newInstance();
			}

			// 将对象放入map中
			map.put(idName, object);
		}
		throw new Exception("xml文件没有子标签!");
	}

	public Object getBean(String name) throws Exception {
		if (StringUtils.isEmpty(name)) {
			throw new Exception("bean name is null");
		}
		return map.get(name);
	}

	private InputStream getResourceAsStream(String path) {
		return this.getClass().getClassLoader().getResourceAsStream(path);
	}

}
