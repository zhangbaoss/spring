package com.zhangbaoss.controller;

import com.zhangbaoss.service.SpringIocXmlService;

/**
 * 〈一句话功能简述〉<br>
 * 〈Spring的控制器〉
 *
 * @author a1638
 * @create 2019/1/11
 * @since 1.0.0
 */
public class SpringIocXmlController {

    private SpringIocXmlService service;

    public void setService(SpringIocXmlService service) {
        this.service = service;
    }
}
