package com.zhangbaoss.service.impl;

import com.zhangbaoss.dao.SpringIocXmlDao;
import com.zhangbaoss.service.SpringIocXmlService;

/**
 * 〈一句话功能简述〉<br>
 * 〈Spring的Service层〉
 *
 * @author a1638
 * @create 2019/1/11
 * @since 1.0.0
 */
public class SpringIocXmlServiceImpl implements SpringIocXmlService {

    private SpringIocXmlDao dao;

    public SpringIocXmlServiceImpl(SpringIocXmlDao dao) {
        this.dao = dao;
    }
}
