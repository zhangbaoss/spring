package com.zhangbao.service.impl;

import com.zhangbao.service.AppService;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/8 17:57
 * @Version 1.0
 **/
@Service
public class AppServiceImpl implements AppService {
    @Override
    public void sout() {
        System.out.println("测试完成!");
    }
}
