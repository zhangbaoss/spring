package com.zhangbao;

import com.zhangbao.controller.AppController;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/9 9:21
 * @Version 1.0
 **/
public class App {

    public static void main(String[] args) {
        /**使用注解版也需要使用xml配置文件来结合使用*/
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:spring-context.xml");
        AppController bean = context.getBean(AppController.class);
        bean.sout();
    }
}
