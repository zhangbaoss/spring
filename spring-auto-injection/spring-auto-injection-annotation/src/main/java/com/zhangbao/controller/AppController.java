package com.zhangbao.controller;

import com.zhangbao.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/8 17:57
 * @Version 1.0
 **/
@Controller
public class AppController {

    /**
     * AutoWired默认是按照类型装配,如果该类型只有一个则直接注入,
     * 如果找到多个则会再按照名字来匹配,如果名字都不对则会抛出异常
     * Resource默认按照bean的名字来匹配,一般配置name属性指定bean名
     * 都可以结合Qualifier注解来指定bean名字
     */
    @Autowired
    @Qualifier("appServiceImpl")
    private AppService appService;

    public void sout() {
        appService.sout();
    }
}
