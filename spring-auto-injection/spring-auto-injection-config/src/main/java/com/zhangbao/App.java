package com.zhangbao;

import com.zhangbao.config.AppConfig;
import com.zhangbao.controller.AppController;
import com.zhangbao.controller.LookupController;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/9 9:47
 * @Version 1.0
 **/
public class App {
    /**
     * 使用java配置类方式不用写xml文件
     * @param args
     */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);
        AppController bean = context.getBean(AppController.class);
        bean.sout();
        LookupController look1 = context.getBean(LookupController.class);
        look1.sout();
        LookupController look2 = context.getBean(LookupController.class);
        look2.sout();
        LookupController look3 = context.getBean(LookupController.class);
        look3.sout();
    }
}
