package com.zhangbao.service.impl;

import com.zhangbao.service.LookupService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Description Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)是原型模式
 * @Author zhangbao
 * @Date 2019/12/9 10:36
 * @Version 1.0
 **/
@Service("lookUpService")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LookupServiceImpl implements LookupService {

    @Override
    public void sout() {
        System.out.println("service: " + this.hashCode());
    }
}
