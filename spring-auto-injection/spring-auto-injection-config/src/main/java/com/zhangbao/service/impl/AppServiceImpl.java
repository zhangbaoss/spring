package com.zhangbao.service.impl;

import com.zhangbao.service.AppService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Description Service的value属性用来指定该bean在容器中的名字,如果不指定,默认bean名为类的首字母小写
 * @Author zhangbao
 * @Date 2019/12/8 17:57
 * @Version 1.0
 **/
@Service(value = "appService")
public class AppServiceImpl implements AppService {
    @Override
    public void sout() {
        System.out.println("测试完成!");
    }
}
