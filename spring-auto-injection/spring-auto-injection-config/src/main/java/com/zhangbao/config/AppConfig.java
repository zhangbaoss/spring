package com.zhangbao.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/9 9:48
 * @Version 1.0
 **/
@Configuration
@ComponentScan(value = "com.zhangbao")
public class AppConfig {
}
