package com.zhangbao.controller;

import com.zhangbao.service.AppService;
import com.zhangbao.service.impl.AppServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/8 17:57
 * @Version 1.0
 **/
@Controller
public class AppController {

    @Autowired
    private AppService appService;

    public void sout() {
        appService.sout();
    }
}
