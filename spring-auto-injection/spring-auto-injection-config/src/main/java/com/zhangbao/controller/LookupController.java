package com.zhangbao.controller;

import com.zhangbao.service.LookupService;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Controller;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/9 10:36
 * @Version 1.0
 **/
@Controller
public class LookupController {

    /**
     * 当依赖的bean为原型模式(prototype)时,
     * 使用lookup来返回不同的bean,否则返回的一直是同一个bean
     * @return
     */
    @Lookup
    public LookupService getLookUpService() {
        return null;
    }

    public void sout() {
        System.out.println("controller: " + this.hashCode());
        getLookUpService().sout();
    }
}
