package com.zhangbao.controller;

import com.zhangbao.service.AppService;
import com.zhangbao.service.impl.AppServiceImpl;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/8 17:57
 * @Version 1.0
 **/
public class AppController {

    /**XML文件中注入时与该属性名无关*/
    private AppService appService;

    public void sout() {
        appService.sout();
    }

    /**XML文件中注入时与该set方法有关*/
    public void setAppService(AppServiceImpl appService) {
        this.appService = appService;
    }

    public AppService getAppService() {
        return appService;
    }
}
