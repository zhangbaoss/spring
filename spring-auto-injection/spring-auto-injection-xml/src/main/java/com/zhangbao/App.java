package com.zhangbao;

import com.zhangbao.controller.AppController;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/8 17:54
 * @Version 1.0
 **/
public class App {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:spring-context.xml");
        AppController bean = context.getBean(AppController.class);
        bean.sout();
    }
}
