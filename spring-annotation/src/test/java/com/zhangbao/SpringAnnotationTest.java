package com.zhangbao;

import com.zhangbao.aop.MathCalculator;
import com.zhangbao.bean.Person;
import com.zhangbao.bean.Pig;
import com.zhangbao.config.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import javax.sql.DataSource;
import java.util.*;

/**
 * 测试类
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class SpringAnnotationTest {
	private ClassPathXmlApplicationContext context;

	private AnnotationConfigApplicationContext annotationConfigApplicationContext;

	private AnnotationConfigApplicationContext lifeCycleContext;

	private AnnotationConfigApplicationContext valueContext;

	private AnnotationConfigApplicationContext profileContext;

	private AnnotationConfigApplicationContext aopContext;

	@Before
	public void init() {
//		context = new ClassPathXmlApplicationContext("beans.xml");
//		annotationConfigApplicationContext = new AnnotationConfigApplicationContext(AnnotationConfig.class);
//		lifeCycleContext = new AnnotationConfigApplicationContext(AnnotationLifeCycleConfig.class);
//		valueContext = new AnnotationConfigApplicationContext(AnnotationValueConfig.class);

		System.out.println("初始化ioc容器...");
	}

	@Test
	public void testPersonXMLBean() {
		Person person = (Person) context.getBean("person");
		System.out.println(person);
	}

	@Test
	public void testPersonAnnotationBean() {
		Person person1 = annotationConfigApplicationContext.getBean(Person.class);
		Person person2 = annotationConfigApplicationContext.getBean(Person.class);
		System.out.println("两个对象是否相等 : " + (person1 == person2));
		String[] names = annotationConfigApplicationContext.getBeanDefinitionNames();
		for (String name : names) {
			System.out.println(name);
		}
	}

	@Test
	public void testCondition() {
		//获取环境变量
		ConfigurableEnvironment environment = annotationConfigApplicationContext.getEnvironment();
		//获取当前操作系统
		String property = environment.getProperty("os.name");
		System.out.println(property);
		String[] beanNamesForType = annotationConfigApplicationContext.getBeanNamesForType(Person.class);
		for (String name : beanNamesForType) {
			System.out.println(name);
		}
		Map<String, Person> beansOfType = annotationConfigApplicationContext.getBeansOfType(Person.class);
		System.out.println(beansOfType);
	}

	@Test
	public void testImport() {
		String[] names = annotationConfigApplicationContext.getBeanDefinitionNames();
		for (String name : names) {
			System.out.println(name);
		}

		//虽然装配的是CustomFactoryBean类型,但实际上是调用getObject方法创建的对象
		Object bean = annotationConfigApplicationContext.getBean("customFactoryBean");
		System.out.println(bean.getClass());

		//如果想要获取工厂对象,需要id前加一个"&"前缀
		Object factoryBean = annotationConfigApplicationContext.getBean("&customFactoryBean");
		System.out.println(factoryBean.getClass());
	}

	@Test
	public void testLifeCycle() {
		lifeCycleContext.getBean("car");
		lifeCycleContext.getBean("cat");
		lifeCycleContext.getBean("dog");
		lifeCycleContext.close();
	}

	@Test
	public void testValue() {
		Pig pig = valueContext.getBean(Pig.class);
		System.out.println(pig);
		valueContext.close();
	}

	@Test
	public void testProfile() {
		profileContext = new AnnotationConfigApplicationContext();
		profileContext.getEnvironment().setActiveProfiles("dev");
		profileContext.register(AnnotationProfileConfig.class);
		profileContext.refresh();

		String[] beanNamesForType = profileContext.getBeanNamesForType(DataSource.class);
		for (String name : beanNamesForType) {
			System.out.println(name);
		}
	}

	@Test
	public void testAop() {
		aopContext = new AnnotationConfigApplicationContext(AnnotationAopConfig.class);
		MathCalculator calculator = aopContext.getBean(MathCalculator.class);
		calculator.div(1, 1);
	}

	@Test
	public void test1() {
		int num = 123;
		long start = System.currentTimeMillis();
		num = reverse(num);
		long end = System.currentTimeMillis();
		System.out.println("时间差:" + (start - end) + ":" + num);
	}

	private int reverse(int num) {
		int hundred = num / 100;
		int ten = (num - hundred * 100) / 10;
		int pcs = (num - hundred * 100) - ten * 10;
		if (hundred != ten) {
			num = pcs * 100 + ten * 10 + hundred;
		}
		return num;
	}

	@Test
	public void test2() {
		int[] arr = {1, 12, 6, 3, 66, 3};
		long start = System.currentTimeMillis();
		maopao(arr);
		xuanze(arr);
		long end = System.currentTimeMillis();
		System.out.println("时间差:" + (end - start));
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ",");
		}
	}

	private void xuanze(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int k = i;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					k = j;
				}
			}
			if (i != k) {
				int temp = arr[i];
				arr[i] = arr[k];
				arr[k] = temp;
			}
		}
	}

	private void maopao(int[] arr) {
		int temp;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
				for (int k = 0; k < arr.length; k++) {
					System.out.print(arr[k] + ",");
				}
				System.out.println();
			}
		}
	}

	@Test
	public void test3() {
		String str = "HeLLo, This is TeSt!";
		str = rever(str);
		System.out.println(str);
	}

	private String rever(String str) {
		char[] chars = str.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] >= 65 && chars[i] <= 90) {
				chars[i] += 32;
			}
		}
		return String.valueOf(chars);
	}

	@Test
	public void test4() {
		boolean b = test4("abelmqzdgfj", "abehqzmdgfj");
		System.out.println(b);
	}

	public static boolean containRepeatChar(String str){
		if(str == null || str.length() == 0){
			return false;
		}
		char[] elements = str.toCharArray();
		for(char e : elements){
			if(str.indexOf(e) != str.lastIndexOf(e)){
				return true;
			}
		}
		return false;
	}

	private boolean test4(String s1, String s2) {
		if (s1 == null && s2 == null) {
			return true;
		} else if (s1 != null && s2 != null) {
			if (s1.length() != s2.length()) {
				return false;
			}
			if (s1.equals(s2)) {
				for (char c : s1.toCharArray()) {
					if (s1.indexOf(c) != s1.lastIndexOf(c)) {
						return true;
					}
				}
			} else {
				int count = 1;

				char[] cs1 = new char[2];
				char[] cs2 = new char[2];
				for (int i = 0; i < s1.length(); i++) {
					if (count > 2) {
						return false;
					}
					if (s1.charAt(i) != s2.charAt(i)) {
						cs1[count - 1] = s1.charAt(i);
						cs2[count - 1] = s2.charAt(i);
						count++;
					}
				}
				if (cs1[0] == cs2[1] && cs1[1] == cs2[0]) {
					return true;
				}
			}
		} else {
			return false;
		}
		return false;
	}

	@Test
	public void test5() {
		new B("a");
	}

	class A {
		public A() {
			System.out.println("A");
		}
	}

	class B extends A {
		public B() {
			System.out.println("B no args");
		}

		public B(String s) {
			this();
			System.out.println("B have args");
		}
	}

	@Test
	public void test6() {
		List<String> list = new ArrayList();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("a");
		System.out.println(list.indexOf("a"));
		Set<String> set = new HashSet<>();
		set.add("a");
		set.add("b");
		set.add("c");
		set.add("d");
		set.add("a");
		set.forEach(System.out::println);
	}
}
