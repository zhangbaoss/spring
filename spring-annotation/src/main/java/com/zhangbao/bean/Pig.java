package com.zhangbao.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 使用注解获取值
 *
 * 初始化Bean顺序:
 * 		1.进入Pig的无参构造器!
 * 		2.进入BeanNameAware接口的setBeanName()方法!
 * 		3.进入BeanFactoryAware接口的setBeanFactory()方法!
 * 		4.进入ApplicationContextAware接口的setApplicationContext()方法!
 * 		5.进入@PostConstruct注解的postConstruct()方法!
 * 		6.进入InitializingBean接口的afterPropertiesSet()方法!
 * 		7.进入BeanPostProcessor接口的postProcessBeforeInitialization()方法!
 * 		8.进入BeanPostProcessor接口的postProcessAfterInitialization()方法!
 * 		9.进入BeanPostProcessor接口的postProcessBeforeInitialization()方法!
 * 		10.进入BeanPostProcessor接口的postProcessAfterInitialization()方法!
 * 		11.进入@PreDestroy注解的preDestroy()方法!
 * 		12.进入DisposableBean接口的destroy()方法!
 *
 * @author zhangbao
 * @date 2019/2/27
 * @since 1.0.0
 */
public class Pig implements BeanNameAware,
		BeanFactoryAware,
		ApplicationContextAware,
		BeanPostProcessor,
		InitializingBean,
		DisposableBean {

	@Value("zhangsan")
	private String name;

	@Value("#{20 - 3}")
	private Integer age;

	@Value("${pig.nickName}")
	private String nickName;

	public Pig() {
		System.out.println("进入Pig的无参构造器!");
	}

	public Pig(String name, Integer age, String nickName) {
		System.out.println("进入Pig的有参构造器!");
		this.name = name;
		this.age = age;
		this.nickName = nickName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Override
	public String toString() {
		return "Pig{" +
				"name='" + name + '\'' +
				", age=" + age +
				", nickName='" + nickName + '\'' +
				'}';
	}

	public void setBeanName(String name) {
		System.out.println("进入BeanNameAware接口的setBeanName()方法!");
	}

	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		System.out.println("进入BeanFactoryAware接口的setBeanFactory()方法!");
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("进入ApplicationContextAware接口的setApplicationContext()方法!");
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("进入BeanPostProcessor接口的postProcessBeforeInitialization()方法!");
		return bean;
	}

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("进入BeanPostProcessor接口的postProcessAfterInitialization()方法!");
		return bean;
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("进入InitializingBean接口的afterPropertiesSet()方法!");
	}

	public void destroy() throws Exception {
		System.out.println("进入DisposableBean接口的destroy()方法!");
	}

	@PostConstruct
	public void postConstruct() {
		System.out.println("进入@PostConstruct注解的postConstruct()方法!");
	}

	@PreDestroy
	public void preDestroy() {
		System.out.println("进入@PreDestroy注解的preDestroy()方法!");
	}
}
