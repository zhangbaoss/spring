package com.zhangbao.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
@Component
public class Cat implements InitializingBean, DisposableBean {
	public Cat() {
		System.out.println("Cat Constructor........");
	}

	public void destroy() throws Exception {
		System.out.println("Cat destory.........");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("Cat afterPropertiesSet.......");
	}
}
