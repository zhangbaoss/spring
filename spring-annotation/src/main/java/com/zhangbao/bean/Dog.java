package com.zhangbao.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class Dog {

	public Dog() {
		System.out.println("Dog Constructor........");
	}

	@PostConstruct
	public void init() {
		System.out.println("Dog init..........");
	}

	@PreDestroy
	public void destory() {
		System.out.println("Dog destory..........");
	}
}
