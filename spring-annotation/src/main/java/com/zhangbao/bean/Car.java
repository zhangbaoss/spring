package com.zhangbao.bean;

/**
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class Car {

	public Car() {
		System.out.println("Car constructor.....");
	}

	public void init() {
		System.out.println("Car init..........");
	}

	public void destroy() {
		System.out.println("Car destroy..........");
	}
}
