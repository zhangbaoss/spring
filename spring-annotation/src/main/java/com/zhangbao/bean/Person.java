package com.zhangbao.bean;

import lombok.*;

/**
 * 人类
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Person {
	private String name;

	private Integer age;

}
