package com.zhangbao.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.StringValueResolver;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * @author zhangbao
 *
 * 切换运行时环境:
 * 	1.在命令行使用动态参数,在虚拟机参数位置加载:-Dspring.profiles.active=test
 * 	2.代码中设置
 * 		profileContext = new AnnotationConfigApplicationContext();
 * 		profileContext.getEnvironment().setActiveProfiles("dev");
 * 		profileContext.register(AnnotationProfileConfig.class);
 * 		profileContext.refresh();
 *
 * @Profile注解作用:指定组件在哪个环境下才能被注册到容器中,不指定值则表示在任何环境下都会注册该组件
 * 	1.加了环境标识的bean,只有在这个环境被激活的时候才能注册到容器中,默认是default环境
 * 	2.写在配置类上,只有是指定的环境时,整个配置类里面的所有配置才会被注册
 *
 * @create 2019/2/28
 * @since 1.0.0
 */
//@Configuration
//@PropertySource("classpath:/jdbc.properties")
public class AnnotationProfileConfig implements EmbeddedValueResolverAware {

	@Value("${jdbc.user}")
	private String user;

	private String driverClass;

	@Profile("test")
	@Bean("testDataSource")
	public DataSource testDataSource(@Value("${jdbc.pswd}") String password) throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setUser(user);
		dataSource.setPassword(password);
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
		dataSource.setDriverClass(driverClass);
		return dataSource;
	}

	@Profile("dev")
	@Bean("devDataSource")
	public DataSource devDataSource(@Value("${jdbc.pswd}") String password) throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setUser(user);
		dataSource.setPassword(password);
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/dev");
		dataSource.setDriverClass(driverClass);
		return dataSource;
	}

	@Profile("pro")
	@Bean("proDataSource")
	public DataSource proDataSource(@Value("${jdbc.pswd}") String password) throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setUser(user);
		dataSource.setPassword(password);
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/pro");
		dataSource.setDriverClass(driverClass);
		return dataSource;
	}

	public void setEmbeddedValueResolver(StringValueResolver resolver) {
		this.driverClass = resolver.resolveStringValue("${jdbc.driverClass}");
	}
}
