package com.zhangbao.config;

import com.zhangbao.bean.Pig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 赋值
 *
 * @PropertySource:如果想要引入外部配置文件,则需要添加该注解,外部的配置文件中的值使用${}获取
 *
 * 1.自动装配@Autowired:
 * 	1.默认优先按照类型从容器中寻找组件进行装配:applicationContext.getBean(xxx.class);
 * 	2.当有多个相同类型的组件时再将属性名当作id再容器中查找进行组件装配
 * 		applilcationContext.getBean("idName");
 * 	3.@Qualifier("idName")注解用来指定需要装配的组件id而不是类型和属性名
 * 	4.自动装配需要赋值好,可以使用@Autowired(required=false)来设置如果没有适合组件可以不装配
 * 	5.@Primary:让Spring进行自动装配时默认使用首选的bean,也可以使用@Qualifier来指定需要装配的bean的名称
 *
 * 2.自动装配@Resource
 * 	这个注解是JSR250规范定义的注解,不支持@Primary扩展,也不支持required=false扩展
 *
 * 3.自动装配@Inject
 * 	该注解是JSR330规范定义的注解,支持@Primary扩展但不支持required=false扩展,需要引入javax.inject依赖
 *
 * 4.@Autowired注解可以标注在方法上、构造器上、属性上.都是从容器中获取参数组件的值
 * 	1.标注在方法上:@Bean + 方法参数:参数从容器中获取,@Autowired注解可以省略
 * 	2.标注在构造器上:如果组件只有一个有参构造器,这个有参构造器的@Autowired注解可以省略,参数位置的组件从容器中获取
 * 	3.标注在属性上
 *
 * 5.如何使用Spring底层的组件如:ApplicationContext,BeanFactory等
 * 	实现xxxAware接口,需要重写一个set方法
 *
 * @author zhangbao
 * @create 2019/2/27
 * @since 1.0.0
 */
//@Configuration
//@PropertySource("classpath:/pig.properties")
public class AnnotationValueConfig {

	@Bean
	public Pig pig() {
		return new Pig();
	}
}
