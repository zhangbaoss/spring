package com.zhangbao.config;

import com.zhangbao.aop.AnnotationAspects;
import com.zhangbao.aop.MathCalculator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * SpringAop配置
 *
 * AOP:指在程序运行期间动态的将某段代码切入到指定方法指定位置中的变成方式
 *
 * 1.导入aop模块,Spring AOP : (spring-aspects)
 * 2.定义一个业务逻辑类(MathCalculator):在业务逻辑运行时将日志打印
 * 		1.方法运行之前
 * 		2.方法运行之后
 * 		3.方法正确返回后
 * 		4.方法抛出异常后
 * 3.定义一个日志切面类(AnnotationAspects):切面类里面的方法需要动态感知MathCalculator.div运行到那种程度了
 * 		通知方法:
 * 			1.前置通知(@Before),before()在目标方法(div)运行之前运行
 * 			2.后置通知(@After),after()在目标方法(div)运行之后运行
 * 			3.返回通知(@AfterReturning),AfterReturning()在目标方法返回值后运行
 * 			4.异常通知(@AfterThrowing),AfterThrowing()在目标方法抛出异常之后运行
 * 			5.环绕通知(@Around)动态代理,手动推进目标方法运行(joinPoint.procced())
 * 4.给切面类的目标方法添加通知注解
 * 5.将切面类和业务逻辑类(目标方法所在类)加入到Spring容器中
 * 6.必须告诉Spring容器哪个类是切面类(给切面类加上一个注解:@Aspect)
 * 7.必须给配置类中加@EnableAspectJAutoProxy(开启基于注解的Aop模式)
 *
 *
 * Aop实现原理:
 * 	1.使用@EnableAspectJAutoProxy开启基于注解的Aop模式
 * 	2.使用@Import(AspectJAutoProxyRegistrar.class)导入一个AspectJAutoProxyRegistrar组件
 * 	3.
 * @author zhangbao
 * @create 2019/2/28
 * @since 1.0.0
 */
@EnableAspectJAutoProxy
@Configuration
public class AnnotationAopConfig {

	@Bean
	public MathCalculator mathCalculator() {
		return new MathCalculator();
	}

	@Bean
	public AnnotationAspects annotationAspects() {
		return new AnnotationAspects();
	}

}
