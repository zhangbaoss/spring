package com.zhangbao.config;

import com.zhangbao.bean.Car;
import com.zhangbao.bean.Dog;
import com.zhangbao.process.CustomBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * bean生命周期配置类
 * 指定初始化和销毁方法:
 * 	 1.@Bean注解中initMethod和destroyMethod
 * 	 2.通过让Bean实现InitializingBean和DisposableBean接口来定义初始化和销毁方法
 * 	 3.使用两个注解:
 * 		1).@PostConstruct:在bean创建完成并且赋值完成后调用初始化方法
 * 		2).@PreDestroy:	容器销毁bean之前通知我们进行清理工作
 * 	4.后置处理器:BeanPostProcessor:在bean初始化前后进行一些处理工作
 * 		1).postProcessBeforeInitialization:前面三种初始化方法之前工作
 * 		2).postProcessAfterInitialization:前面三种初始化方法之后工作
 *
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
//@Configuration
//@ComponentScan("com.zhangbao.bean")
public class AnnotationLifeCycleConfig {

	/**
	 *
	 * bean生命周期,创建-->初始化-->销毁
	 * 创建:
	 * 	单例:ioc容器创建时创建对象
	 * 	多例:使用时创建对象
	 * 初始化:
	 * 	对象创建完成,并赋值好调用初始化方法
	 * 销毁:
	 * 	单实例:容器关闭的时候
	 * 	多实例:容器只帮你创建,不帮你管理
	 * @return
	 */
	@Bean(initMethod = "init", destroyMethod = "destroy")
	public Car car() {
		return new Car();
	}

	@Bean
	public Dog dog() {
		return new Dog();
	}

	@Bean
	public CustomBeanPostProcessor customBeanPostProcessor() {
		return new CustomBeanPostProcessor();
	}
}
