package com.zhangbao.config;

import com.zhangbao.bean.Color;
import com.zhangbao.bean.Person;
import com.zhangbao.condition.LinuxConditional;
import com.zhangbao.condition.WindowsConditional;
import com.zhangbao.factory.CustomFactoryBean;
import com.zhangbao.filter.CustomTypeFilter;
import com.zhangbao.selector.CustomImportBeanDefinitionRegistrar;
import com.zhangbao.selector.CustomImportSelector;
import com.zhangbao.service.AnnotationService;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * 注解配置类
 *
 * @Configuration注解告诉Spring容器这是一个注解类
 *
 * @ComponentScans:定义多个扫描规则
 * @ComponentScan:定义单个扫描规则,jdk8以上可以重复添加该注解,以下的使用@ComponentScans注解定义多个规则
 * 		value:扫描的包
 * 		excludeFilters:扫描时按照指定规则排除哪些组件
 * 			type:指定规则类型为FilterType.ANNOTATION
 * 			value:添加了Controller注解的类不进行扫描
 * 		includeFilters:扫描时按照指定规则只扫描哪些组件
 * 		useDefaultFilters:如果想要扫描时按照指定规则只扫描特定组件需要设置该属性为false
 *
 *	扫描规则:
 *	FilterType.ANNOTATION:根据注解扫描
 *	FilterType.ASSIGNABLE_TYPE:根据指定类型进行扫描
 *	FilterType.ASPECTJ:根据ASPECTJ表达式进行扫描
 *	FilterType.REGEX:根据正则表达式进行扫描
 *	FilterType.CUSTOM:根据自定义规则进行扫描
 *
 * @Import:快速导入组件,id默认为组件全类名
 * 导入组件方式:
 * 	1.包扫描+组件标注注解(@Controller/@Service...)[只针对自己写的类]
 * 	2.@Bean注解[导入第三方包里组件]
 * 	3.@Import注解,可以直接放类,也可以放selector选择器和ImportBeanDefinitionRegistrar类
 * 	4.使用Spring提供的FactoryBean创建对象
 *
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
//@Configuration
/*@ComponentScans(value = {
		@ComponentScan(value = "com.zhangbao", includeFilters = {
			*//*@ComponentScan.Filter(type = FilterType.ANNOTATION,
					value = {Controller.class}),
			@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
					value = {AnnotationService.class}),*//*
			@ComponentScan.Filter(type = FilterType.CUSTOM,
					value = {CustomTypeFilter.class})
		}, useDefaultFilters = false)
})*/
//@Import({Color.class, CustomImportSelector.class, CustomImportBeanDefinitionRegistrar.class})
public class AnnotationConfig {

	/**
	 * @Bean注解给容器注入一个bean,类型为返回值类型,id默认为方法名,可以使用name属性更改
	 * @Scope:设置对象的作用域
	 * 		singleton:单例对象,ioc容器创建时就会创建该单例对象,放入ioc容器中,使用时直接从容器中获取
	 * 		prototype:多例对象,ioc容器创建时不会创建对象,每次从ioc容器中获取对象时
	 * 			才会创建对象并放入到ioc容器中,每次获取时都会创建新的对象
	 * 		request:每次请求创建一个对象
	 * 		session:每个session创建一个对象
	 * @Lazy:懒加载,单实例对象在ioc容器创建时就已经创建好了,使用懒加载可以让ioc容器创建时不创建对象,
	 * 		第一次使用该对象的时候再进行创建,只针对单例
	 * @return
	 */
//	@Scope("prototype")
	@Lazy
	@Bean(name = "person")
	public Person person() {
		System.out.println("创建对象放入ioc容器中...");
		return new Person("lisi", 20);
	}

	/**
	 * @Conditional:放在方法上根据条件判断是否加载某个bean,放在类上表示满足一定条件这个类下的所有配置才会生效
	 * @return
	 */
	@Conditional({WindowsConditional.class})
	@Bean("bill")
	public Person person1() {
		return new Person("bill", 60);
	}

	@Conditional(LinuxConditional.class)
	@Bean("linus")
	public Person person2() {
		return new Person("linus", 49);
	}

	/**
	 * 虽然装配的是CustomFactoryBean类型,但实际上是调用getObject方法创建的对象
	 * 如果想要获取工厂对象,需要id前加一个"&"前缀
	 * @return
	 */
	@Bean
	public CustomFactoryBean customFactoryBean() {
		return new CustomFactoryBean();
	}
}
