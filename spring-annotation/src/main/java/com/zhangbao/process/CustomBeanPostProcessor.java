package com.zhangbao.process;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * 自定义后置处理器
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class CustomBeanPostProcessor implements BeanPostProcessor {
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("调用初始化之前方法: bean : " + bean + "==>beanName : " + beanName);
		return bean;
	}

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("调用初始化之后方法: bean : " + bean + "==>beanName : " + beanName);
		return bean;
	}
}
