package com.zhangbao.filter;

import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;

/**
 * 自定义扫描规则
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class CustomTypeFilter implements TypeFilter {

	/**
	 *
	 * @param metadataReader 读取到的当前正在扫描的类的信息
	 * @param metadataReaderFactory 获取到其它类的信息(比如说父类或接口)
	 * @return true则表示匹配成功
	 * @throws IOException
	 */
	public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {

		//获取注解信息
		AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();

		//获取类信息
		ClassMetadata classMetadata = metadataReader.getClassMetadata();

		//获取当前类资源路径信息
		Resource resource = metadataReader.getResource();

		//获取当前类名
		String className = classMetadata.getClassName();
		if (className.contains("er")) {
			return true;
		}

		return false;
	}
}
