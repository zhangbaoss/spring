package com.zhangbao.condition;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * windows条件判断
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class WindowsConditional implements Condition {

	/**
	 *
	 * @param context 判断条件能使用的上下文
	 * @param metadata 标注了Conditation注解的注释信息
	 * @return
	 */
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

		//ioc容器使用的beanFactory
		ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();

		//获取类加载器
		ClassLoader classLoader = context.getClassLoader();

		//获取bean定义的注册类
		BeanDefinitionRegistry registry = context.getRegistry();

		//获取当前运行时环境变量
		Environment environment = context.getEnvironment();

		//获取操作系统名字
		String property = environment.getProperty("os.name");

		if (property.contains("Windows")) {
			return true;
		}

		return false;
	}
}
