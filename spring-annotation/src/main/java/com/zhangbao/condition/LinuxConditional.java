package com.zhangbao.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * linux条件判断
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class LinuxConditional implements Condition {

	/**
	 *
	 * @param context 判断条件能使用的上下文
	 * @param metadata 标注了Conditation注解的注释信息
	 * @return
	 */
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

		//获取当前运行时环境变量
		Environment environment = context.getEnvironment();

		//获取操作系统名字
		String property = environment.getProperty("os.name");

		if (property.contains("linux")) {
			return true;
		}

		return false;
	}
}
