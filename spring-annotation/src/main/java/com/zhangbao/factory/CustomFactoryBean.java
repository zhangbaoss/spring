package com.zhangbao.factory;

import com.zhangbao.bean.Color;
import org.springframework.beans.factory.FactoryBean;

/**
 * 自定义FactoryBean
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class CustomFactoryBean implements FactoryBean<Color> {
	public Color getObject() throws Exception {
		System.out.println("调用CustomFactoryBean的getObject方法");
		return new Color();
	}

	public Class<?> getObjectType() {
		return Color.class;
	}

	/**
	 * true:表示是单例,容器中只有一份
	 * false:多例,每次获取都会调用一次getObject方法
	 * @return
	 */
	public boolean isSingleton() {
		return false;
	}
}
