package com.zhangbao.service.impl;

import com.zhangbao.service.AnnotationService;
import org.springframework.stereotype.Service;

/**
 * Service
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
@Service
public class AnnotationServiceImpl implements AnnotationService {
}
