package com.zhangbao.aop;

/**
 * 计算器
 *
 * @author zhangbao
 * @create 2019/2/28
 * @since 1.0.0
 */
public class MathCalculator {

	public int div(int i, int j) {
		return i/j;
	}

}
