package com.zhangbao.selector;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * Bean定义注册
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class CustomImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

	/**
	 *
	 * @param importingClassMetadata 导入的类信息
	 * @param registry Bean定义注册类
	 */
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
		boolean definition1 = registry.containsBeanDefinition("com.zhangbao.bean.Red");
		boolean definition2 = registry.containsBeanDefinition("com.zhangbao.bean.Bule");
		if (definition1 && definition2) {
			RootBeanDefinition beanDefinition = new RootBeanDefinition("com.zhangbao.bean.Yellow");
			registry.registerBeanDefinition("yellow", beanDefinition);
		}
	}
}
