package com.zhangbao.selector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 自定义选择器
 *
 * @author zhangbao
 * @create 2019/2/25
 * @since 1.0.0
 */
public class CustomImportSelector implements ImportSelector {
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		return new String[]{"com.zhangbao.bean.Red", "com.zhangbao.bean.Bule"};
//		return new String[0];
	}
}
