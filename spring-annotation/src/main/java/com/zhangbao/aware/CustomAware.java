package com.zhangbao.aware;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;

/**
 * 使用Spring底层组件
 *
 * @author zhangbao
 * @create 2019/2/28
 * @since 1.0.0
 */
@Component
public class CustomAware implements ApplicationContextAware, BeanFactoryAware, EmbeddedValueResolverAware {

	private ApplicationContext applicationContext;

	private BeanFactory beanFactory;

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

	public void setEmbeddedValueResolver(StringValueResolver resolver) {
		String str = resolver.resolveStringValue("Hello : ${os.name}, I'm #{20*10}");
		System.out.println("解析后的字符串" + str);
	}
}
