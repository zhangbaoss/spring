package com.zhangbao;

import static org.junit.Assert.assertTrue;

import com.zhangbao.config.SpringTxConfig;
import com.zhangbao.controller.SpringTxController;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testTx() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringTxConfig.class);
        SpringTxController controller = context.getBean(SpringTxController.class);
        String testTx = controller.testTx();
        System.out.println(testTx);
    }
}
