package com.zhangbao.service.impl;

import com.zhangbao.dao.SpringTxDao;
import com.zhangbao.service.SpringTxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/5/21 上午 10:40
 * @Version 1.0
 **/
@Service
public class SpringTxServiceImpl implements SpringTxService {

    @Autowired
    private SpringTxDao springTxDao;

    @Override
    public String testTx() {
        System.out.println("Service ==> success");
        return springTxDao.testTx();
    }
}
