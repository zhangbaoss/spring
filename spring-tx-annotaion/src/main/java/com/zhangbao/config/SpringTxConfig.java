package com.zhangbao.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * @Description Spring事务配置文件
 *
 * 开启事务:
 *  1.添加@EnableTransactionManagement注解,功能和aop的@Enablexxx注解功能类似,
 *  与spring的xml文件中<tx:annotation-driven/>起同样作用
 *  2.添加事务管理器用来控制事务PlatformTransactionManager
 *  3.添加@Tancation注解用来开启事务
 *
 * @Author zhangbao
 * @Date 2019/5/21 上午 10:12
 * @Version 1.0
 **/
@EnableTransactionManagement
@ComponentScan("com.zhangbao")
@Configuration
public class SpringTxConfig {

    @Bean
    public DataSource dataSource() throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser("root");
        dataSource.setPassword("931112");
        dataSource.setDriverClass("com.mysql.jdbc.Driver");
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/spring_annotation");
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() throws PropertyVetoException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
        return jdbcTemplate;
    }

    /**
     * 添加事务管理器用来控制事务,事务管理器需要声明管理哪个数据库
     * @return
     * @throws PropertyVetoException
     */
    @Bean
    public PlatformTransactionManager transactionManager() throws PropertyVetoException {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource());
        return transactionManager;
    }
}
