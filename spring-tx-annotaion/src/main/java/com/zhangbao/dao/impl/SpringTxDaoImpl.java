package com.zhangbao.dao.impl;

import com.zhangbao.dao.SpringTxDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;
import java.util.UUID;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/5/21 上午 10:43
 * @Version 1.0
 **/
@Repository
public class SpringTxDaoImpl implements SpringTxDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String testTx() {
        System.out.println("Dao ==> success");
        String sql = "INSERT INTO spring_annotation_tx_user(user_name, user_age) VALUES (?, ?)";
        String userName = UUID.randomUUID().toString().substring(0, 5);
        int age = new Random().nextInt(20) + 1;
        jdbcTemplate.update(sql, userName, age);
        //此处出现异常查看数据库中数据是否插入
        int i = 10 / 0;
        return "数据插入成功!";
    }
}
