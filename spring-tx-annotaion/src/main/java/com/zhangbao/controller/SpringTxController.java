package com.zhangbao.controller;

import com.zhangbao.service.SpringTxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/5/21 上午 10:37
 * @Version 1.0
 **/
@Controller
public class SpringTxController {

    @Autowired
    private SpringTxService springTxService;

    public String testTx() {
        System.out.println("Controller ==> success");
        return springTxService.testTx();
    }
}
