package com.zhangbaoss.service.impl;

import com.zhangbaoss.dao.SpringIocAnnotationDao;
import com.zhangbaoss.ioc.annotation.ExtAutowired;
import com.zhangbaoss.ioc.annotation.ExtService;
import com.zhangbaoss.service.SpringIocAnnotationService;

/**
 * 〈一句话功能简述〉<br>
 * 〈Spring的Service层〉
 *
 * @author a1638
 * @create 2019/1/11
 * @since 1.0.0
 */
@ExtService("springIocAnnotationService")
public class SpringIocAnnotationServiceImpl implements SpringIocAnnotationService {

	@ExtAutowired
	private SpringIocAnnotationDao springIocAnnotationDao;

	@Override
	public void test() {
		System.out.println("到达service层");
		springIocAnnotationDao.test();
	}
}
