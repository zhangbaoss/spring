package com.zhangbaoss.ioc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description 组件扫描路径
 * Target注解表示能将该注解放到类上还是方法上,属性上等
 * Retention注解表示该注解在运行时存在还是编译后还存在还是源代码上存在
 * @Author zhangbao
 * @Date 2019/12/12 16:54
 * @Version 1.0
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExtComponentScan {

    String value() default "";
}
