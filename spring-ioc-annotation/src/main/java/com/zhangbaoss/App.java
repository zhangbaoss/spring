package com.zhangbaoss;

import com.zhangbaoss.config.AppConfig;
import com.zhangbaoss.controller.SpringIocAnnotationController;
import com.zhangbaoss.ioc.ExtAnnotationConfigApplicationContext;

/**
 * 〈一句话功能简述〉<br>
 * 〈程序入口类〉
 *
 * @author a1638
 * @create 2019/1/12
 * @since 1.0.0
 */
public class App {

	public static void main(String[] args) throws Exception {
		ExtAnnotationConfigApplicationContext app =
				new ExtAnnotationConfigApplicationContext(AppConfig.class);
		SpringIocAnnotationController springIocAnnotationController =
				(SpringIocAnnotationController) app.getBean("springIocAnnotationController");
		springIocAnnotationController.test();
	}
}
