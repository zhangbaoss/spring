package com.zhangbaoss.config;

import com.zhangbaoss.ioc.annotation.ExtComponentScan;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/12 17:15
 * @Version 1.0
 **/
@ExtComponentScan("com.zhangbaoss")
public class AppConfig {
}
