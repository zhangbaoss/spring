package com.zhangbao.annotation;

import com.zhangbao.registrar.CustomImportBeanDefinitionRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description 用来标志哪些包下面的接口需要代理,并将代理类放入spring容器中
 * @Author zhangbao
 * @Date 2019/12/17 14:36
 * @Version 1.0
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(CustomImportBeanDefinitionRegistrar.class)
public @interface CustomMapperScan {

    String value();
}
