package com.zhangbao.registrar;

import com.zhangbao.bean.CustomFactoryBean;
import com.zhangbao.dao.CustomAppDao;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/17 14:39
 * @Version 1.0
 **/
public class CustomImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // 获取所有需要被代理的接口,对其进行代理并放入Spring容器中,此处写死为CustomAppDao

        String[] names = registry.getBeanDefinitionNames();


        // 1.获取Spring容器中beanDefinitionMap里面的类型为CustomAppDao的BeanDefinition
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(CustomAppDao.class);
        GenericBeanDefinition beanDefinition = (GenericBeanDefinition) builder.getBeanDefinition();

        // 2.将com.zhangbao.dao.CustomAppDao作为参数传给CustomFactoryBean的构造函数构造对象
        beanDefinition.getConstructorArgumentValues().addGenericArgumentValue("com.zhangbao.dao.CustomAppDao");
        /**
         * 3.将beanDefinition中的BeanClass属性修改为CustomFactoryBean类型,
         * 此后spring获取customAppDao时获取的都是CustomFactoryBean中的getObject()方法所返回的对象
         */
        beanDefinition.setBeanClass(CustomFactoryBean.class);

        // 4.将修改后的beanDefinition重新方法Spring容器中
        registry.registerBeanDefinition("customAppDao", beanDefinition);
    }
}
