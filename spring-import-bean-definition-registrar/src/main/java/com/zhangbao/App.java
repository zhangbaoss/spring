package com.zhangbao;

import com.zhangbao.config.AppConfig;
import com.zhangbao.service.CustomAppService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/17 14:36
 * @Version 1.0
 **/
public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);

        CustomAppService service = context.getBean(CustomAppService.class);
        service.testSpring();
    }
}
