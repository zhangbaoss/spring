package com.zhangbao.bean;

import com.zhangbao.handler.CustomInvocationHandler;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;

/**
 * @Description 该类不能添加@Component注解,
 * 如果添加@Component注解则需要添加无参构造方法,否则Spring创建该类失败
 * @Author zhangbao
 * @Date 2019/12/17 15:53
 * @Version 1.0
 **/
@Component
public class CustomFactoryBean implements FactoryBean {

    private Class clazz;

    public CustomFactoryBean(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    public Object getObject() throws Exception {
        ClassLoader loader = this.getClass().getClassLoader();
        Class[] classes = new Class[]{clazz};
        // 返回代理对象
        return Proxy.newProxyInstance(loader, classes, new CustomInvocationHandler());
    }

    @Override
    public Class<?> getObjectType() {
        return clazz;
    }
}
