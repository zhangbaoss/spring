package com.zhangbao.dao;

import org.springframework.stereotype.Repository;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/17 15:46
 * @Version 1.0
 **/
@Repository
public interface CustomAppDao {

    void testSpring();
}
