package com.zhangbao.service.impl;

import com.zhangbao.dao.CustomAppDao;
import com.zhangbao.service.CustomAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/17 15:48
 * @Version 1.0
 **/
@Service
public class CustomAppServiceImpl implements CustomAppService {

    @Autowired
    private CustomAppDao dao;

    @Override
    public void testSpring() {
        System.out.println("CustomAppServiceImpl.testSpring()");
        dao.testSpring();
    }
}
