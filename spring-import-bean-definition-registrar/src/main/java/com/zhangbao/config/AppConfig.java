package com.zhangbao.config;

import com.zhangbao.annotation.CustomMapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/17 14:36
 * @Version 1.0
 **/
@Configuration
@ComponentScan("com.zhangbao")
@CustomMapperScan("com.zhangbao.dao")
public class AppConfig {
}
