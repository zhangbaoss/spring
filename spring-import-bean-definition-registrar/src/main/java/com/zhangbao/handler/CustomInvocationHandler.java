package com.zhangbao.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Description TODO
 * @Author zhangbao
 * @Date 2019/12/17 14:38
 * @Version 1.0
 **/
public class CustomInvocationHandler implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("测试Proxy...");
        // 此处是需要代理的对象的实现方法结果
        return null;
    }
}
